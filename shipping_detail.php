<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Shipping Details :: onCart</title>
</head>

<div class="container">
	<div class="register">
		<form id="form_cart_detail" name="form_cart_detail" method="post" action="">
			<div class="col-md-6 register-buttom-grid">
				<div class="mation">
				<h1>Cart Details</h1>
					<table >
						<tr>
							<th>Item</th>
							<th>Qty</th>		
							<th>Unit Prices</th>
							<th>Product Weight</th>
							<th>Subtotal</th>
						</tr>
						<?php
							$res = "SELECT * FROM tblcart, tblproduct WHERE tblcart.user_email = '".$_SESSION['email']."' AND tblcart.prod_code = tblproduct.prod_code ORDER BY prod_name ASC";
							$checkres = mysql_query($res, $dbLink);
							if ($checkres) {
								for($i=0; $i<mysql_num_rows($checkres); $i++) {
									$reg = mysql_fetch_array($checkres);
						?>
									<tr>
										<td><h5><?php echo $reg['prod_name']; ?></h5></td>
										<td align="center"><?php echo $reg['cart_prod_qty']; ?></td>
										<td>RM <?php echo $reg['prod_final_sell_price']; ?></td>
										<td><?php echo $reg['cart_prod_weight']; ?> kg</td>
										<td id="sub">RM <?php echo $reg['cart_sub_total']; ?></td>
									</tr>
						<?php
									$total += $reg['cart_sub_total'];
									$weight += $reg['cart_prod_weight'];
								}
								$ship_fee = ($total > 70) ? 0 : (ceil($weight)*15);
								$ship_fee = number_format((float)$ship_fee, 2, '.', '');
								$total = number_format((float)$total, 2, '.', '');
								$grand_total = $total+$ship_fee;
								$grand_total = number_format((float)$grand_total, 2, '.', '');
							}
						?>
						<tr style="border-top-style: solid;"> <td></td> <td></td> <td></td> <td align="right">Total: </td> <td>RM <?php echo $total ?></td> </tr>
						<tr> <td></td> <td></td> <td></td> <td align="right">Shipping Fees: </td> <td style="border-bottom-style: solid;">RM <?php echo $ship_fee ?></td> </tr>
						<tr> <td></td> <td></td> <td></td> <td align="right">Grand Total: </td> <td style="border-width: 5px; border-bottom-style: double;">RM <?php echo $grand_total ?></td> </tr>
					</table>
					<button type="submit" name="btnedit" class="to-buy">Edit</button><br/>
				</div>
			</div>
		</form>

		<?php
			$res = "SELECT user_fullname, user_email, user_mphone, user_address, user_city, user_postcode, user_state FROM tbluser WHERE user_email = '".$_SESSION['email']."'";
			$checkres = mysql_query($res, $dbLink);
			if ($checkres) {
				$reg = mysql_fetch_array($checkres);
			}
		?>

		<form id="form_recipient_addr" name="form_recipient_addr" method="post" action="">
			<div class="col-md-6 register-top-grid" style="border-left-style: dashed;">
				<div class="mation">
					<h1>Shipping Address</h1>
					<span id="text_get">Name</span>
					<input type="text" name="txtname" required="required" value="<?php echo $reg['user_fullname']; ?>" />
				 
					<span>Email Address</span>
					<input type="email" name="txtemail" required="required" value="<?php echo $reg['user_email']; ?>" />

					<span>Phone</span>
					<input type="text" name="txtmobile" required="required" value="<?php echo $reg['user_mphone']; ?>" />

					<span>Address</span>
					<input type="text" name="txtaddr" required="required" value="<?php echo $reg['user_address']; ?>" />

					<div class="col-md-6">
						<span>City</span>
						<input type="text" name="txtcity" required="required" value="<?php echo $reg['user_city']; ?>" />
					</div>

					<div class="col-md-6">
						<span>Postcode</span>
						<input type="text" name="txtpostcode" required="required" value="<?php echo $reg['user_postcode']; ?>" />
					</div>

					<span>State</span>
					<select class="form-group-lg form-control" style="margin-top: 5px;" name="sel_state" required="required">
						<option value="Unknown State">Please Select...</option>
						<?php
						for($i=0; $i<count($states); $i++) {
							if($reg['user_state'] == $states[$i]) {
								echo "<option selected='selected' value='".$states[$i]."'>".$states[$i]."</option>";
							}
							else {
								echo "<option value='".$states[$i]."'>".$states[$i]."</option>";
							}
						}
						?>
   					</select>
				</div>
				<div class="register-but">
					<input type="submit" value="PLACE ORDER" name="btnplace">
					<div class="clearfix"> </div>
				</div>
			</div>
			<div class="clearfix"> </div>
		</form>
	</div>
</div>

<?php
if(isset($_POST['btnplace'])) {
	$t = microtime(true);
	$micro = sprintf("%06d",($t - floor($t)) * 1000000);
	$d = new DateTime( date('y-m-d H:i:s.'.$micro, $t) );
	$order_num = $d->format("ymdHisu");
	$order_date = date("Y-m-d H:i:s");

	$cartres = "SELECT * FROM tblcart WHERE user_email = '".$_SESSION['email']."'";
	$checkcartres = mysql_query($cartres, $dbLink);
	if ($checkcartres) {
		for($i=0; $i<mysql_num_rows($checkcartres); $i++) {
			$reg = mysql_fetch_array($checkcartres);
			$movecart = "INSERT INTO tblorder(order_num, prod_code, user_email, order_prod_qty, order_prod_weight, order_sub_total, order_status, order_date_add) VALUES('".$order_num."', '".$reg['prod_code']."', '".$reg['user_email']."', '".$reg['cart_prod_qty']."', '".$reg['cart_prod_weight']."', '".$reg['cart_sub_total']."', '0', '".$order_date."')";
			$movecartres = mysql_query($movecart, $dbLink);
			$delcart = "DELETE FROM tblcart WHERE user_email='".$_SESSION['email']."'";
			$sqldelcart = mysql_query($delcart, $dbLink);
			echo $prodres = "SELECT prod_qty FROM tblproduct WHERE prod_code = '".$reg['prod_code']."'";
			$checkprodres = mysql_query($prodres, $dbLink);
			$qty = mysql_fetch_array($checkprodres);
			if($qty['prod_qty'] != '-1') {
				$updqty = $qty['prod_qty'] - $reg['cart_prod_qty'];
				$updprodres = "UPDATE tblproduct SET prod_qty = '".$updqty."' WHERE prod_code = '".$reg['prod_code']."'";
				$checkupdprodres = mysql_query($updprodres, $dbLink);
			}
			$total_weight += $reg['cart_prod_weight'];
		}
	}
	$shipres = "INSERT INTO tblship(order_num, ship_weight, ship_name, ship_email, ship_mphone, ship_address, ship_city, ship_postcode, ship_state) VALUES('".$order_num."', '".$total_weight."', '".ucwords(trim($_POST['txtname']))."', '".trim($_POST['txtemail'])."', '".trim($_POST['txtmobile'])."', '".trim($_POST['txtaddr'])."', '".trim($_POST['txtcity'])."', '".trim($_POST['txtpostcode'])."', '".trim($_POST['sel_state'])."')";
	$checkshipres = mysql_query($shipres, $dbLink);
	echo "<script>alert('Order successfully.'); location='index.php?id=invoice&oid=".$order_num."';</script>";
}
?>

<?php
if(isset($_POST['btnedit'])) {
	echo "<script>location='index.php?id=checkout';</script>";
}
?>