<?php session_start(); ?>
<?php if(!empty($_SESSION['email']) && ($_SESSION['level'] == "admin" || $_SESSION['level'] == "employee")) { ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>My Profile :: onCart</title>
</head>

<?php
	$res = "SELECT * FROM tbluser WHERE user_id = '".$_GET['uid']."'";
	$checkres = mysql_query($res, $dbLink);
	if ($checkres) {
		$reg = mysql_fetch_array($checkres);
	}
?>

<?php
	if(isset($_POST['btnreject'])) {
		$updateUser = "UPDATE tbluser SET user_status = '1' WHERE user_id = '".$_POST['btnreject']."'";
		$updateResult = mysql_query($updateUser, $dbLink);
		if($updateResult) 
			echo "<script>alert('Reject Successfully!'); location='index.php?id=apr_cmpy';</script>";
		else
			echo "<script>alert('Reject Failed!');</script>";
	}
	if(isset($_POST['btnapprove'])) {
		$updateUser = "UPDATE tbluser SET user_status = '2' WHERE user_id = '".$_POST['btnapprove']."'";
		$updateResult = mysql_query($updateUser, $dbLink);
		if($updateResult) 
			echo "<script>alert('Approve Successfully!'); location='index.php?id=apr_cmpy';</script>";
		else
			echo "<script>alert('Approve Failed!');</script>";
	}
?>

<div class="container">
	<div class="register">
		<h1>Company Profile</h1>
			<form id="form_profile" name="form_profile" method="post" action="">
			<div class="col-md-6  register-top-grid">
				<div class="mation">
					<span id="text_get">Company or Seller Name</span>
					<input type="text" name="txtname" readonly="readonly" value="<?php echo $reg['user_fullname']; ?>" />

					<span>Business License</span>
					<input type="text" name="txtbsnum" readonly="readonly" value="<?php echo $reg['user_ic']; ?>" />

					<span>GST Registration Number</span>
					<input type="text" name="txtgstnum" readonly="readonly" value="<?php echo $reg['user_gst_code']; ?>" />

					<span>Mobile Phone</span>
					<input type="text" name="txtmobile" readonly="readonly" value="<?php echo $reg['user_mphone']; ?>" />
				 
					<span>Email Address</span>
					<input type="email" name="txtemail" readonly="readonly" value="<?php echo $reg['user_email']; ?>" />
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class=" col-md-6 register-bottom-grid">
				<div class="mation">
					<span>Contact Person</span>
					<input type="text" name="txtcp" readonly="readonly" value="<?php echo $reg['user_contact_person']; ?>" />

					<span>Contact Person Number</span>
					<input type="text" name="txtcpnum" readonly="readonly" value="<?php echo $reg['user_contact_person_phone']; ?>" />

					<span>Address</span>
					<input type="text" name="txtaddr" readonly="readonly" value="<?php echo $reg['user_address']; ?>" />

					<div class="col-md-6">
						<span>City</span>
						<input type="text" name="txtcity" readonly="readonly" value="<?php echo $reg['user_city']; ?>" />
					</div>

					<div class="col-md-6">
						<span>Postcode</span>
						<input type="text" name="txtpostcode" readonly="readonly" value="<?php echo $reg['user_postcode']; ?>" />
					</div>

					<span>State</span>
					<input type="text" name="txtstate" readonly="readonly" value="<?php echo $reg['user_state']; ?>" />
				</div>
			</div>
			<div class="clearfix"> </div>
				
			<div class="register-but">
				<button class="btn btn-danger btn-lg" name="btnreject" onclick="return confirm('Are you sure want to reject?')" value="<?php echo $reg['user_id']; ?>">Reject</button>
				<button class="btn btn-success btn-lg" name="btnapprove" value="<?php echo $reg['user_id']; ?>">Approve</button>
				<div class="clearfix"> </div>
			</div>
		</form>
	</div>
</div>

<?php
}
else {
	echo "<script>alert('Access Denied'); location='index.php';</script>";
}
?>