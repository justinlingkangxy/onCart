<?php session_start(); ?>
<?php if(!empty($_SESSION['email']) && ($_SESSION['level'] == "admin" || $_SESSION['level'] == "employee")) { ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Payment :: onCart</title>
</head>

<div class="container">
	<div class="register">
		<div class="col-md-3"></div>
		<form id="form_receipt_img" name="form_receipt_img" method="post" action="">
			<div class="col-md-6 register-top-grid">
				<?php
					$getimg = "SELECT img_name FROM tblimage WHERE img_code = '".$_GET['oid']."'";
					$getimgResult = mysql_query($getimg, $dbLink);
					if(mysql_num_rows($getimgResult) > 0) {
						$img_name = mysql_fetch_array($getimgResult);
						echo "<div align='center'><img src='slip_images/".$img_name['img_name']."' width='auto' height='200px' alt=''></div>";
					}
					else {
						echo "<div align='center'><img src='images/no_image.jpg' width='auto' height='200px' alt=''></div>";
					}
				?>
			</div>
			<div class="clearfix"> </div>
				
			<div class="register-but">
				<button class="btn btn-danger btn-lg" name="btnreject" onclick="return confirm('Are you sure want to reject?')" value="<?php echo $_GET['oid']; ?>">Reject</button>
				<button class="btn btn-success btn-lg" name="btnapprove" value="<?php echo $_GET['oid']; ?>">Approve</button>
				<div class="clearfix"> </div>
			</div>
		</form>
		<div class="col-md-3"></div>
	</div>
</div>

<?php
	if(isset($_POST['btnreject'])) {
		$updateOrder = "UPDATE tblorder SET order_status = '1' WHERE order_num = '".$_POST['btnreject']."'";
		$updateResult = mysql_query($updateOrder, $dbLink);
		if($updateResult) 
			echo "<script>alert('Reject Successfully!'); location='index.php?id=apr_order';</script>";
		else
			echo "<script>alert('Reject Failed!');</script>";
	}
	if(isset($_POST['btnapprove'])) {
		$updateOrder = "UPDATE tblorder SET order_status = '2' WHERE order_num = '".$_POST['btnapprove']."'";
		$updateResult = mysql_query($updateOrder, $dbLink);
		if($updateResult) 
			echo "<script>alert('Approve Successfully!'); location='index.php?id=apr_order';</script>";
		else
			echo "<script>alert('Approve Failed!');</script>";
	}
?>
<?php
}
else {
	echo "<script>alert('Access Denied'); location='index.php';</script>";
}
?>