<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Order Report :: onCart</title>
</head>

<?php
if($_GET['pg'] == "") {
	$page = 0;
}
else {
	$page = ($_GET['pg']*10)-10;
}
if(empty($_GET['mode'])) {
	if(!empty($_GET['uid'])) {
		$res = "SELECT order_num, order_date_add, tblorder.user_email AS buyer_email, tblproduct.user_email AS seller_email, tbluser.user_id AS seller_id, tbluser.user_fullname as seller_name, SUM(order_sub_total) AS grand_total FROM tblorder, tblproduct, tbluser WHERE DAY(order_date_add) = '".$_GET['d']."' AND MONTHNAME(order_date_add) = '".$_GET['m']."' AND YEAR(order_date_add) = '".$_GET['y']."' AND tbluser.user_id = '".$_GET['uid']."' AND tbluser.user_email = tblproduct.user_email AND tblproduct.prod_code = tblorder.prod_code AND order_status > 1 GROUP BY order_num";
	}
	else {
		$res = "SELECT * FROM tblorder WHERE user_email = '".$_SESSION['email']."' GROUP BY order_num ORDER BY order_date_add DESC";
	}
}
else {
	$res = "SELECT order_num, order_date_add, tblorder.user_email AS buyer_email, tblproduct.user_email AS seller_email, tbluser.user_id AS seller_id, tbluser.user_fullname as seller_name, SUM(order_sub_total) AS grand_total FROM tblorder, tblproduct, tbluser WHERE DAY(order_date_add) = '".$_GET['d']."' AND MONTHNAME(order_date_add) = '".$_GET['m']."' AND YEAR(order_date_add) = '".$_GET['y']."' AND tbluser.user_email = tblproduct.user_email AND tblproduct.prod_code = tblorder.prod_code AND order_status > 1 GROUP BY order_num";
}
$checkres = mysql_query($res, $dbLink);
$sellerreg = mysql_fetch_array($checkres);
$num = mysql_num_rows($checkres);
$max_page = ceil($num/10);
$res .= " LIMIT ".$page.",10";
$checkres = mysql_query($res, $dbLink);
?>

<div align="center">
<div class="products">
	<div class="container">
		<h1><?php if(!empty($_GET['mode'])) echo "Customer"; else if(!empty($_GET['uid'])) echo $sellerreg['seller_name']; else echo "My" ?> Order Report</h1>
	</div>
</div>
<form action="" method="post" accept-charset="utf-8">
	<?php if(!empty($_GET['uid']) || !empty($_GET['mode'])) { ?>
		<table class="table">
			<thead>
				<tr>
					<th>No.</th>
					<th>Order Number</th>
					<th>Customer</th>
					<th>Total Sales</th>
				</tr>
			</thead>
			<tbody id="table_content">
				<?php
					if(mysql_num_rows($checkres) > 0) {
						for($i=0; $i<mysql_num_rows($checkres); $i++) {
							$reg = mysql_fetch_array($checkres);
							$custres = "SELECT user_fullname, user_id FROM tbluser WHERE user_email = '".$reg['buyer_email']."'";
							$checkcustres = mysql_query($custres, $dbLink);
							$custreg = mysql_fetch_array($checkcustres);
				?>
							<tr>
								<td><?php echo ($i+1)."."; ?></td>
								<td onclick="location='index.php?id=order_detail&oid=<?php echo $reg['order_num']; ?><?php if(empty($_GET['mode'])) echo "&sid=".$reg['seller_id']; else echo "&mode=cust"; ?>&d=<?php echo $_GET['d']; ?>&m=<?php echo $_GET['m']; ?>&y=<?php echo $_GET['y']; ?>'"><?php echo $reg['order_num']; ?></td>
								<td onclick="location='index.php?id=cust_profile&uid=<?php echo $custreg['user_id']; ?>'"><?php echo $custreg['user_fullname']; ?></td>
								<td>RM <?php echo $reg['grand_total']; ?></td>
							</tr>
				<?php
							$total += $reg['grand_total'];
						}
						$total = number_format((float)$total, 2, '.', '');
				?>
							<tr style="border-top-style: solid;"> <td></td> <td></td> <td align="right">Total: </td> <td style="border-width: 5px; border-bottom-style: double;">RM <?php echo $total ?></td> </tr>
				<?php
					}
					else {
						echo "<tr><td></td><td></td><td>No Order Record(s) Found.</td><td></td></tr>";
					}
				?>
			</tbody>
		</table>
	<?php } else { ?>
		<table class="table">
			<thead>
				<tr>
					<th>No.</th>
					<th>Date Added</th>
					<th>Order Number</th>
					<th>Status</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="table_content">
				<?php
					if(mysql_num_rows($checkres) > 0) {
						for($i=0; $i<mysql_num_rows($checkres); $i++) {
							$reg = mysql_fetch_array($checkres);
							$date_time = explode(' ', $reg['order_date_add']);
							$date = $date_time[0];
							$time = $date_time[1];
							if($reg['order_status'] == 0) {
								$ostatus = "Payment Pending";
							}
							else if($reg['order_status'] == 1) {
								$ostatus = "Order Rejected";
							}
							else if($reg['order_status'] == 2) {
								$ostatus = "Process";
							}
							else if($reg['order_status'] == 3) {
								$ostatus = "Shipped";
							}
				?>
							<tr>
								<td><?php echo ($i+1)."."; ?></td>
								<td><span title="<?php echo $time; ?>"><?php echo $date; ?></span></td>
								<td><?php echo $reg['order_num']; ?></td>
								<td><?php echo $ostatus; ?></td>
								<td>
									<a class="btn btn-info btn-xs" href="index.php?id=order_detail&oid=<?php echo $reg['order_num']; ?>" title="View Details"><i class="fa fa-list-alt"></i></a>
									<?php if($reg['order_status'] == 0) { ?>
									<a class="btn btn-success btn-xs" href="index.php?id=payment&oid=<?php echo $reg['order_num']; ?>" title="Pay Order"><i class="fa fa-money"></i></a>
									<?php } ?>
								</td>
							</tr>
				<?php 	}
					}
					else {
						echo "<tr><td></td><td></td><td>No Order Record(s) Found.</td><td></td><td></td></tr>";
					}
				?>
			</tbody>
		</table>
	<?php } ?>
	</div> <div class='clearfix'> </div>
	<?php if($max_page > 1) { ?>
	<div align="center">
		<ul class="pagination">
			<li><a href="index.php?id=list_order&pg=1" <?php if($_GET['pg'] == 1 || empty($_GET['pg'])) echo "class='not-active'"; ?>><i class="fa fa-angle-double-left"></i></a></li>
			<li><a href="<?php if(empty($_GET['pg']) || $_GET['pg'] == 1) echo 'index.php?id=list_order&pg=1'; else echo 'index.php?id=list_order&pg='.($_GET['pg']-1); ?>" <?php if($_GET['pg'] == 1 || empty($_GET['pg'])) echo "class='not-active'"; ?>><i class="fa fa-angle-left"></i></a></li>
			<?php
				for($i=1; $i<=$max_page; $i++) {
					if(empty($_GET['pg']) && $i == 1) {
						echo "<li><a class='active' href='index.php?id=list_order&pg=".$i."'>".$i."</a></li>";
					}
					else if(!empty($_GET['pg']) && $_GET['pg'] == $i) {
						echo "<li><a class='active' href='index.php?id=list_order&pg=".$i."'>".$i."</a></li>";
					}
					else {
						echo "<li><a href='index.php?id=list_order&pg=".$i."'>".$i."</a></li>";
					}
				}
			?>
			<li><a href="<?php if(empty($_GET['pg']) || $_GET['pg'] == 1) echo 'index.php?id=list_order&pg=2'; else echo 'index.php?id=list_order&pg='.($_GET['pg']+1); ?>" <?php if($_GET['pg'] == $max_page) echo "class='not-active'"; ?>><i class="fa fa-angle-right"></i></a></li>
			<li><a href="index.php?id=list_order&pg=<?php echo $max_page ?>" <?php if($_GET['pg'] == $max_page) echo "class='not-active'"; ?>><i class="fa fa-angle-double-right"></i></a></li>
		</ul>
	</div>
	<?php } ?>
</form>
</div>