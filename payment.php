<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Payment :: onCart</title>
</head>

<div class="container">
	<div class="register">
		<div class="col-md-6 register-buttom-grid">
			<div class="mation">
				<table>
					<tr>
						<th colspan="2">Bank Transfer To PUBLIC BANK</th>
					</tr>
					<tr>
						<td width="30%">Account Name:</td>
						<td>onCart</td>
					</tr>
					<tr>
						<td width="30%">Account No.:</td>
						<td>4848 1000 8888 8888</td>
					</tr>
				</table>
				<div class="clearfix"> </div>
				<a class="btn btn-info btn-1" href="index.php?id=invoice&oid=<?php echo $_GET['oid']; ?>">View Invoice</a>
			</div>
		</div>

		<form id="form_receipt_img" name="form_receipt_img" method="post" action="" enctype="multipart/form-data">
			<div class="col-md-6 register-top-grid" style="border-left-style: dashed;">
				<div class="mation">
					<h1>Upload Bank-in Slip</h1>
					<input type="file" name="img" value="" placeholder="choose your slip image">
					<div class="register-but">
						<input type="submit" name="btnupload" value="Upload">
					</div>
				</div>
				<div class="clearfix"> </div>
				<?php
				if(isset($_POST['btnupload'])) {
					echo $file_name = $_FILES['img']['name'];

					if($file_name != "") {
						$file_type = $_FILES['img']['type'];
						$allow_ext = array("jpg", "jpeg", "png", "gif");
						$ext = end(explode(".", $file_name));

						if(in_array(strtolower($ext), $allow_ext)) { //check file is invalid type*/
							$file_size = $_FILES['img']['size'];

							if($file_size < 10000000) { //check file is less than 10MB
								$file_new_name = $_GET['oid'].".".$ext; //rename file
								$file_tmp_name = $_FILES['img']['tmp_name'];
								$path = "slip_images/".$file_new_name;

								if(move_uploaded_file($file_tmp_name, $path)) {
									$upimg = "INSERT INTO tblimage(img_name, img_code) VALUES('".$file_new_name."', '".$_GET['oid']."')";
									$upimgResult = mysql_query($upimg, $dbLink);
									echo "<script>location='index.php?".$_SERVER['QUERY_STRING']."';</script>";
								}
								else {
									echo "<script>alert('Image upload failed!');</script>";
								}
							}
							else {
								echo "<script>alert('Image should be less than 10MB!');</script>";
							}
						}
						else {
							echo "<script>alert('Invalid image type!');</script>";
						}
					}
					else {
						echo "<script>alert('Please select an image!');</script>";
					}
				}

				$getimg = "SELECT img_name FROM tblimage WHERE img_code = '".$_GET['oid']."'";
				$getimgResult = mysql_query($getimg, $dbLink);
				if(mysql_num_rows($getimgResult) > 0) {
					$img_name = mysql_fetch_array($getimgResult);
					echo "<div align='center'><img src='slip_images/".$img_name['img_name']."' width='auto' height='200px' alt=''></div>";
				}
				else {
					echo "<div align='center'><img src='images/no_image.jpg' width='auto' height='200px' alt=''></div>";
				}
				?>
			</div>
			<div class="clearfix"> </div>
		</form>
	</div>
</div>