<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Products :: onCart</title>
</head>

<form id="form_cart" name="form_cart" method="post" action="">
<div class="container">
	<div class="check-out">
		<h1>Checkout</h1>
		<?php
			$res = "SELECT * FROM tblcart, tblproduct WHERE tblcart.user_email = '".$_SESSION['email']."' AND tblcart.prod_code = tblproduct.prod_code ORDER BY prod_name ASC";
			$checkres = mysql_query($res, $dbLink);
			if(mysql_num_rows($checkres) > 0) {
		?>
			<table>
				<tr>
					<th></th>
					<th>Item</th>
					<th>Quantities</th>		
					<th>Unit Prices</th>
					<th>Product Weight</th>
					<th>Subtotal</th>
				</tr>
				<?php
				if ($checkres) {
					for($i=0; $i<mysql_num_rows($checkres); $i++) {
						$reg = mysql_fetch_array($checkres);
				?>
						<tr>
							<td><button type="submit" class="btn btn-danger btn-xs" name="btndel" title="Remove item" onclick="return confirm('Are you sure want to remove <?php echo $reg['prod_name']; ?> from shopping cart?')" value="<?php echo $reg['prod_code']; ?>"><i class="fa fa-times"></i></button></td>
							<td class="ring-in" style="cursor:pointer" onclick="location='index.php?id=single&pcode=<?php echo $reg['prod_code']; ?>'"><a href="index.php?id=single&pcode=<?php echo $reg['prod_code'] ?>" class="at-in">
								<?php
								$getimg = "SELECT img_name FROM tblimage WHERE img_code = '".$reg['prod_code']."' AND img_name LIKE '".$reg['prod_code']."1%'";
								$getimgResult = mysql_query($getimg, $dbLink);
								$img_name = mysql_fetch_array($getimgResult);
								if(!empty($img_name['img_name'])) {
									echo "<img class='img-responsive' src='prod_images/".$img_name['img_name']."' width='auto' height='100px' alt=''>";
								}
								else {
									echo "<img class='img-responsive' src='images/no_image.jpg' width='auto' height='100px' alt=''>";
								}
								?>
								</a>
								<div class="sed">
									<h5><?php echo $reg['prod_name']; ?></h5>
									<p><?php echo $reg['prod_desc']; ?></p>
								</div>
								<div class="clearfix"> </div>
							</td>
							<td class="check" align="center"><input type="number" id="txtqty<?php echo $i; ?>" name="txtqty<?php echo $i; ?>" value="<?php echo $reg['cart_prod_qty']; ?>" min="1" <?php if($reg['prod_qty'] > 0) echo "max='".$reg['prod_qty']."'"; ?>></td>
							<td>RM <?php echo $reg['prod_final_sell_price']; ?></td>
							<td><?php echo $reg['cart_prod_weight']; ?> kg</td>
							<td id="sub">RM <?php echo $reg['cart_sub_total']; ?></td>
						</tr>
				<?php
						$total += $reg['cart_sub_total'];
						$weight += $reg['cart_prod_weight'];
					}
					$ship_fee = ($total > 70) ? 0 : (ceil($weight)*15) ;
					$ship_fee = number_format((float)$ship_fee, 2, '.', '');
					$total = number_format((float)$total, 2, '.', '');
					$grand_total = $total+$ship_fee;
					$grand_total = number_format((float)$grand_total, 2, '.', '');
				}
				?>
				<tr style="border-top-style: solid;"> <td></td> <td></td> <td></td> <td></td> <td align="right">Total: </td> <td>RM <?php echo $total ?></td> </tr>
				<tr> <td></td> <td></td> <td></td> <td></td> <td align="right">Shipping Fees: </td> <td style="border-bottom-style: solid;">RM <?php echo $ship_fee ?></td> </tr>
				<tr> <td></td> <td></td> <td></td> <td></td> <td align="right">Grand Total: </td> <td style="border-width: 5px; border-bottom-style: double;">RM <?php echo $grand_total ?></td> </tr>
			</table>
			<button type="submit" name="btnupd" class="to-buy">Update</button>
			<button type="submit" name="btnbuy" class="to-buy pull-right">PROCEED TO PURCHASE</button>
			<div class="clearfix"> </div>
		<?php
			}
			else {
				echo "<h2>No item in shopping cart.</h2>";
			}
		?>
	</div>
</div>
</form>

<?php
if(isset($_POST['btndel'])) {
	$delcart = "DELETE FROM tblcart WHERE prod_code='".$_POST['btndel']."'";
	$sqldelcart = mysql_query($delcart, $dbLink);
	echo "<script>location='index.php?".$_SERVER['QUERY_STRING']."';</script>";
}
?>

<?php
if(isset($_POST['btnupd'])) {
	$res = "SELECT * FROM tblcart, tblproduct WHERE tblcart.user_email = '".$_SESSION['email']."' AND tblcart.prod_code = tblproduct.prod_code ORDER BY prod_name ASC";
	$checkres = mysql_query($res, $dbLink);
	if ($checkres) {
		for($i=0; $i<mysql_num_rows($checkres); $i++) {
			$reg = mysql_fetch_array($checkres);
			if($_POST['txtqty'.$i] > $reg['prod_qty'] && $reg['prod_qty'] != '-1') {
				$qty = $reg['prod_qty'];
			}
			else {
				$qty = $_POST['txtqty'.$i];
			}
				$sub_total = $qty * $reg['prod_final_sell_price'];
				$prod_weight = $qty * $reg['prod_weight'];
				$updcart = "UPDATE tblcart SET cart_prod_qty = '".$qty."', cart_prod_weight = '".$prod_weight."', cart_sub_total = '".$sub_total."', cart_date_upd = '".date("Y-m-d H:i:s")."' WHERE prod_code = '".$reg['prod_code']."'";
				$updcartres = mysql_query($updcart, $dbLink);
				echo "<script>location='index.php?id=checkout';</script>";
		}
	}
}
?>

<?php
if(isset($_POST['btnbuy'])) {
	$res = "SELECT * FROM tblcart, tblproduct WHERE tblcart.user_email = '".$_SESSION['email']."' AND tblcart.prod_code = tblproduct.prod_code ORDER BY prod_name ASC";
	$checkres = mysql_query($res, $dbLink);
	if ($checkres) {
		for($i=0; $i<mysql_num_rows($checkres); $i++) {
			$reg = mysql_fetch_array($checkres);
			if($_POST['txtqty'.$i] > $reg['prod_qty'] && $reg['prod_qty'] != '-1') {
				$qty = $reg['prod_qty'];
			}
			else {
				$qty = $_POST['txtqty'.$i];
			}
				$sub_total = $qty * $reg['prod_final_sell_price'];
				$updcart = "UPDATE tblcart SET cart_prod_qty = '".$qty."', cart_sub_total = '".$sub_total."', cart_date_upd = '".date("Y-m-d H:i:s")."' WHERE prod_code = '".$reg['prod_code']."'";
				$updcartres = mysql_query($updcart, $dbLink);
				echo "<script>location='index.php?id=ship_detail';</script>";
		}
	}
}
?>