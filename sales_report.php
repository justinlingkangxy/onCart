<?php session_start(); ?>
<?php if(!empty($_SESSION['email']) && ($_SESSION['level'] == "admin" || $_SESSION['level'] == "employee" || $_SESSION['level'] == "seller")) { ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php if(!empty($_GET['uid'])) echo "Seller's "; if(empty($_GET['uid']) && empty($_GET['mode'])) echo "My "; if($_GET['dtype'] == 'd') echo $_GET['m']." Daily"; else echo "Monthly" ?> Sales Report :: onCart</title>
</head>

<?php
if(isset($_POST['btnback'])) {
	echo "<script>window.history.go(-2);</script>";
}

if($_GET['pg'] == "") {
	$page = 0;
}
else {
	$page = ($_GET['pg']*10)-10;
}
$res = "SELECT YEAR(order_date_add) AS year, MONTHNAME(order_date_add) AS monthname, MONTH(order_date_add) AS month, DAY(order_date_add) AS day, order_num, order_date_add, tblorder.user_email, tbluser.user_fullname, SUM(order_sub_total) AS grand_total FROM tblorder, tblproduct, tbluser WHERE tblorder.order_status > 1 AND YEAR(order_date_add) = '".$_GET['y']."'";
if($_GET['dtype'] == 'd') {
	$res .= " AND MONTHNAME(order_date_add) = '".$_GET['m']."'";
}
if(empty($_GET['mode'])) {
	if(!empty($_GET['uid'])) {
		$sellerres = "SELECT user_email, user_fullname, user_id FROM tbluser WHERE user_id = '".$_GET['uid']."'";
		$checksellerres = mysql_query($sellerres, $dbLink);
		if($checksellerres) {
			$sellerreg = mysql_fetch_array($checksellerres);
			$res .= " AND tblproduct.user_email = '".$sellerreg['user_email']."'";
		}
	}
	else {
		$sellerres = "SELECT user_email, user_fullname, user_id FROM tbluser WHERE user_email = '".$_SESSION['email']."'";
		$checksellerres = mysql_query($sellerres, $dbLink);
		if($checksellerres) {
			$sellerreg = mysql_fetch_array($checksellerres);
			$res .= " AND tblproduct.user_email = '".$_SESSION['email']."'";
		}
	}
}
$res .= " AND tblorder.prod_code = tblproduct.prod_code AND tblorder.user_email = tbluser.user_email GROUP BY YEAR(order_date_add), MONTH(order_date_add)";
if($_GET['dtype'] == 'd') {
	$res .= ", DAY(order_date_add)";
}
$res .= " ORDER BY order_date_add ASC";
$checkres = mysql_query($res, $dbLink);
$num = mysql_num_rows($checkres);
$max_page = ceil($num/10);
$res .= " LIMIT ".$page.",10";
$checkres = mysql_query($res, $dbLink);
?>

<div align="center">
<div class="products">
	<div class="container">
		<h1><?php if(!empty($_GET['uid'])) echo $sellerreg['user_fullname']."<br/>"; if(empty($_GET['uid']) && empty($_GET['mode'])) echo "My "; if($_GET['dtype'] == 'd') echo $_GET['m']." Daily"; else echo date('Y')." Monthly" ?> Sales Report</h1>
	</div>
</div>
<form action="" method="post" accept-charset="utf-8">
	<?php if(empty($_GET['uid'])) { ?>
	<div class="col-md-6"></div>
	<div class="col-md-6">
		<select class="form-group-lg form-control" style="width: auto;" name="sel_year" onchange="location='index.php?id=admin_sales_report&y=' + this.value;">
			<?php
			$yearres = "SELECT YEAR(order_date_add) AS year FROM tblorder WHERE order_status > 1 GROUP BY YEAR(order_date_add)";
			$checkyearres = mysql_query($yearres, $dbLink);
			for($i=0; $i<mysql_num_rows($checkyearres); $i++) {
				$yearreg = mysql_fetch_array($checkyearres);
				if($yearreg['year'] == $_GET['y']) {
					echo "<option selected='selected' value='".$yearreg['year']."'>".$yearreg['year']."</option>";
				}
				else {
					echo "<option value='".$yearreg['year']."'>".$yearreg['year']."</option>";
				}
			}
			?>
		</select>
	</div>
	<br/><br/>
	<?php } ?>
	<table class="table">
		<thead>
			<tr>
				<th>No.</th>
				<th><?php if($_GET['dtype'] == 'd') echo "Day"; else echo "Month" ?></th>
				<th>Total Sales</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody id="table_content">
			<?php
				if(mysql_num_rows($checkres) > 0) {
					for($i=0; $i<mysql_num_rows($checkres); $i++) {
						$reg = mysql_fetch_array($checkres);
			?>
						<tr>
							<td><?php echo ($i+1); ?></td>
							<?php if($_GET['dtype'] == 'd') { ?>
								<td><span title="<?php echo $reg['monthname']; ?>"><?php echo $reg['day']; ?></span></td>
								<td>RM <?php echo $reg['grand_total']; ?></td>
								<td>
									<a class="btn btn-info btn-xs" href="index.php?id=list_order<?php if(empty($_GET['mode'])) echo "&uid=".$sellerreg['user_id']; else echo "&mode=cust"; ?>&y=<?php echo $reg['year'] ?>&m=<?php echo $reg['monthname'] ?>&d=<?php echo $reg['day'] ?>" title="View Details"><i class="fa fa-list-alt"></i></a>
								</td>
							<?php } else { ?>
								<td><span title="<?php echo $reg['year']; ?>"><?php echo $reg['monthname']; ?></span></td>
								<td>RM <?php echo $reg['grand_total']; ?></td>
								<td>
									<a class="btn btn-info btn-xs" href="index.php?id=sales_report<?php if(empty($_GET['mode'])) echo "&uid=".$sellerreg['user_id']; else echo "&mode=cust"; ?>&dtype=d&y=<?php echo $reg['year'] ?>&m=<?php echo $reg['monthname'] ?>" title="View Details"><i class="fa fa-list-alt"></i></a>
								</td>
							<?php } ?>
						</tr>
			<?php
						$total += $reg['grand_total'];
					}
					$total = number_format((float)$total, 2, '.', '');
			?>
						<tr style="border-top-style: solid;"> <td></td> <td align="right">Total: </td> <td style="border-width: 5px; border-bottom-style: double;">RM <?php echo $total ?></td> <td></td>  </tr>
			<?php
				}
				else {
					echo "<tr><td></td><td></td><td>No Record(s) Found.</td><td></td><td></td></tr>";
				}
			?>
		</tbody>
	</table>
	<button type="submit"class="btn btn-info btn-1"  name="btnback">Back</button>
	</div>
	<div class='clearfix'> </div>
	<?php if($max_page > 1) { ?>
	<div align="center">
		<ul class="pagination">
			<li><a href="index.php?id=sales_report&y=<?php echo $_GET['y']; ?>&pg=1" <?php if($_GET['pg'] == 1 || empty($_GET['pg'])) echo "class='not-	active'"; ?>><i class="fa fa-angle-double-left"></i></a></li>
			<li><a href="<?php if(empty($_GET['pg']) || $_GET['pg'] == 1) echo 'index.php?id=sales_report&y='.$_GET['y'].'&pg=1'; else echo 'index.	php?id=sales_report&y='.$_GET['y'].'&pg='.($_GET['pg']-1); ?>" <?php if($_GET['pg'] == 1 || empty($_GET['pg'])) echo "class='not-	active'"; ?>><i class="fa fa-angle-left"></i></a></li>
			<?php
				for($i=1; $i<=$max_page; $i++) {
					if(empty($_GET['pg']) && $i == 1) {
						echo "<li><a class='active' href='index.php?id=sales_report&y=".$_GET['y']."&pg=".$i."'>".$i."</a></li>";
					}
					else if(!empty($_GET['pg']) && $_GET['pg'] == $i) {
						echo "<li><a class='active' href='index.php?id=sales_report&y=".$_GET['y']."&pg=".$i."'>".$i."</a></li>";
					}
					else {
						echo "<li><a href='index.php?id=sales_report&y=".$_GET['y']."&pg=".$i."'>".$i."</a></li>";
					}
				}
			?>
			<li><a href="<?php if(empty($_GET['pg']) || $_GET['pg'] == 1) echo 'index.php?id=sales_report&y='.$_GET['y'].'&pg=2'; else echo 'index.	php?id=sales_report&y='.$_GET['y'].'&pg='.($_GET['pg']+1); ?>" <?php if($_GET['pg'] == $max_page) echo "class='not-active'"; ?>><i 	class="fa fa-angle-right"></i></a></li>
			<li><a href="index.php?id=sales_report&y=<?php echo $_GET['y']; ?>&pg=<?php echo $max_page ?>" <?php if($_GET['pg'] == $max_page) echo "	class='not-active'"; ?>><i class="fa fa-angle-double-right"></i></a></li>
		</ul>
	</div>
	<?php } ?>
</form>
<br/>
</div>
<?php
}
else {
	echo "<script>alert('Access Denied'); location='index.php';</script>";
}
?>