<?php session_start(); ?>
<?php if(!empty($_SESSION['email']) && ($_SESSION['level'] == "admin" || $_SESSION['level'] == "employee")) { ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php echo $_GET['y']." "; ?>Monthly User Summary Report :: onCart</title>
</head>

<?php
if($_GET['pg'] == "") {
	$page = 0;
}
else {
	$page = ($_GET['pg']*10)-10;
}

$buyerres = "SELECT COUNT(user_id) AS total_user FROM tbluser WHERE user_level = 0 AND user_status > 1 AND YEAR(user_register_date) = '".$_GET['y']."'";
$checkbuyerres = mysql_query($buyerres, $dbLink);
$sellerres = "SELECT COUNT(user_id) AS total_user FROM tbluser WHERE user_level = 2 AND user_status > 1 AND YEAR(user_register_date) = '".$_GET['y']."'";
$checksellerres = mysql_query($sellerres, $dbLink);
?>

<div align="center">
<div class="products">
	<div class="container">
		<h1><?php echo $_GET['y']." "; ?> User Summary Report</h1>
	</div>
</div>
<form action="" method="post" accept-charset="utf-8" style="width:40%">
	<div class="col-md-6"></div>
	<div class="col-md-6">
		<select class="form-group-lg form-control" style="width: auto;" name="sel_year" onchange="location='index.php?id=user_summary&y=' + this.value;">
			<?php
			$yearres = "SELECT YEAR(user_register_date) AS year FROM tbluser WHERE user_status > 1 AND user_level < 3 GROUP BY YEAR(user_register_date)";
			$checkyearres = mysql_query($yearres, $dbLink);
			for($i=0; $i<mysql_num_rows($checkyearres); $i++) {
				$yearreg = mysql_fetch_array($checkyearres);
				if($yearreg['year'] == $_GET['y']) {
					echo "<option selected='selected' value='".$yearreg['year']."'>".$yearreg['year']."</option>";
				}
				else {
					echo "<option value='".$yearreg['year']."'>".$yearreg['year']."</option>";
				}
			}
			?>
		</select>
	</div>
	<br/><br/>
	<table class="table">
		<thead>
			<tr>
				<th>User Type</th>
				<th>Total</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody id="table_content">
			<?php
				$buyerreg = mysql_fetch_array($checkbuyerres);
				$sellerreg = mysql_fetch_array($checksellerres);
			?>
			<tr>
				<td>Buyer</td>
				<td><?php echo $buyerreg['total_user']; ?></td>
				<td>
					<a class="btn btn-info btn-xs" href="index.php?id=user_summary_details&level=0&y=<?php echo $_GET['y']; ?>" title="View Details"><i class="fa fa-list-alt"></i></a>
				</td>
			</tr>
			<tr>
				<td>Seller</td>
				<td><?php echo $sellerreg['total_user']; ?></td>
				<td>
					<a class="btn btn-info btn-xs" href="index.php?id=user_summary_details&level=2&y=<?php echo $_GET['y']; ?>" title="View Details"><i class="fa fa-list-alt"></i></a>
				</td>
			</tr>
		</tbody>
	</table>
	</div> <div class='clearfix'> </div>
</form>
</div>

<?php
}
else {
	echo "<script>alert('Access Denied'); location='index.php';</script>";
}
?>