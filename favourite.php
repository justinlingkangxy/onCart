<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>My Favourite List :: onCart</title>
</head>

<?php
if(isset($_POST['btnaddcart'])) {
	if(empty($_SESSION['email'])) {
		echo "<script>alert('Please login.'); location='index.php?id=login';</script>";
	}
	else {
		$countcart = "SELECT COUNT(cart_id) AS RecordNum FROM tblcart WHERE prod_code = '".$_POST['btnaddcart']."' AND user_email = '".$_SESSION['email']."'";
		$countcartres = mysql_query($countcart, $dbLink);
		$Row = mysql_fetch_array($countcartres);
		if($Row['RecordNum'] > 0) {
			$checkQuery = "SELECT * FROM tblcart, tblproduct WHERE tblcart.prod_code = '".$_POST['btnaddcart']."' AND tblcart.prod_code = tblproduct.prod_code AND tblcart.user_email = '".$_SESSION['email']."'";
			$checkQueryRes = mysql_query($checkQuery, $dbLink);
			if(mysql_num_rows($checkQueryRes) > 0) {
				$reg = mysql_fetch_array($checkQueryRes);
				$new_qty = ($reg['cart_prod_qty']+1);
				$sub_total = $reg['prod_final_sell_price']*$new_qty;
				$prod_weight = $reg['prod_weight']*$new_qty;
				$updcart = "UPDATE tblcart SET cart_prod_qty = '".$new_qty."', cart_prod_weight = '".$prod_weight."', cart_sub_total = '".$sub_total."', cart_date_upd = '".date("Y-m-d H:i:s")."' WHERE prod_code = '".$reg['prod_code']."'";
				$updcartresult = mysql_query($updcart, $dbLink);
				echo "<script>location='index.php?".$_SERVER['QUERY_STRING']."';</script>";
			}
		}
		else {
			$checkQuery = "SELECT prod_final_sell_price, prod_weight FROM tblproduct WHERE prod_code = '".$_POST['btnaddcart']."'";
			$checkQueryRes = mysql_query($checkQuery, $dbLink);
			$reg = mysql_fetch_array($checkQueryRes);
			$sqlCart = "INSERT INTO tblcart(prod_code, user_email, cart_prod_qty, cart_prod_weight, cart_sub_total, cart_date_add) VALUES('".$_POST['btnaddcart']."', '".$_SESSION['email']."', '1', '".$reg['prod_weight']."', '".$reg['prod_final_sell_price']."', '".date("Y-m-d H:i:s")."')";
			$sqlCartResult = mysql_query($sqlCart, $dbLink);
			echo "<script>location='index.php?".$_SERVER['QUERY_STRING']."';</script>";
		}
	}
}
if(isset($_POST['btnremove'])) {
	$delwish = "DELETE FROM tblwish WHERE prod_code='".$_POST['btnremove']."'";
	$sqldelwish = mysql_query($delwish, $dbLink);
	echo "<script>location='index.php?".$_SERVER['QUERY_STRING']."';</script>";
}
?>

<div class="products">
	<div class="container">
		<h1>My Favourite List</h1>
		<form action="" method="post" name="form_prod_add" id="form_prod_add">
		<div class="col-md-9">
		<?php
			$cat = $_GET['cat'];
			if($_GET['pg'] == "") {
				$page = 0;
			}
			else {
				$page = ($_GET['pg']*9)-9;
			}
			$res = "SELECT * FROM tblproduct, tblwish WHERE tblproduct.prod_code = tblwish.prod_code AND tblwish.user_email = '".$_SESSION['email']."'";
			$res .= " ORDER BY wish_date_add DESC";
			$checkwishres = mysql_query($res, $dbLink);
			$num = mysql_num_rows($checkwishres);
			$max_page = ceil($num/9);
			$res .= " LIMIT ".$page.",9";
			$checkres = mysql_query($res, $dbLink);
			if(mysql_num_rows($checkres) > 0) {
		?>
				<div class="content-top1">
		<?php
				for($i=0; $i<mysql_num_rows($checkres); $i++) {
					$reg = mysql_fetch_array($checkres);
		?>
					<div class="col-md-4 col-md3">
						<div class="col-md1 simpleCart_shelfItem">
							<a href="index.php?id=single&pcode=<?php echo $reg['prod_code'] ?>">
								<?php
								$getimg = "SELECT img_name FROM tblimage WHERE img_code = '".$reg['prod_code']."' AND img_name LIKE '".$reg['prod_code']."1%'";
								$getimgResult = mysql_query($getimg, $dbLink);
								$img_name = mysql_fetch_array($getimgResult);
								if(!empty($img_name['img_name'])) {
									echo "<img class='img-product' src='prod_images/".$img_name['img_name']."' alt=''>";
								}
								else {
									echo "<img class='img-product' src='images/no_image.jpg' alt=''>";
								}
								?>
							</a>
							<h3><a href="index.php?id=single&pcode=<?php echo $reg['prod_code'] ?>"><?php echo $reg['prod_name']; ?></a></h3>
							<h2 class="item_price pull-left" style="font-family: Arial;">RM <?php echo $reg['prod_final_sell_price']; ?></h2>
							<button type="submit" name="btnaddcart" title="Add to Cart" class="btn btn-xs btn-info pull-right" value="<?php echo $reg['prod_code']; ?>"><i class="fa fa-shopping-cart"></i></button>
							<button type="submit" name="btnremove" title="Remove from Favourite" onclick="return confirm('Are you sure want to remove <?php echo $reg['prod_name']; ?> from favourite?')" class="btn btn-xs btn-danger pull-right" value="<?php echo $reg['prod_code']; ?>"><i class="fa fa-times"></i></button>
							<div class="clearfix"> </div>
						</div>
					</div>
		<?php
					if(($i+1) % 3 == 0)
						echo "<div class='clearfix'> </div> </div> <div class='content-top1'>";
				}
				echo "</div> <div class='clearfix'> </div>";
		?>
		<?php
			} else {
				echo "<br/><h3 class='h_text_style' align='center'>No Favourite Product(s) Found.</h3>";
			}
		?>
		</div>
		<div class="col-md-3 product-bottom">
			<div class="product-bottom">
				<h3 class="cate">Top Sales</h3>
				<?php
					$topres = "SELECT tblorder.prod_code, SUM(order_prod_qty), prod_final_sell_price, prod_name FROM tblorder, tblproduct WHERE order_status > 1 AND tblorder.prod_code = tblproduct.prod_code GROUP BY prod_code ORDER BY SUM(order_prod_qty) DESC, order_sub_total DESC, order_date_add DESC LIMIT 0, 4";
					$checktopres = mysql_query($topres, $dbLink);
					if(mysql_num_rows($checktopres) > 0) {
						for($i=0; $i<4; $i++) {
						$topreg = mysql_fetch_array($checktopres);
				?>
						<div class="product-go">
							<div class="fashion-grid">
								<a href="index.php?id=single&pcode=<?php echo $topreg['prod_code'] ?>">
									<?php
									$getimg = "SELECT img_name FROM tblimage WHERE img_code = '".$topreg['prod_code']."' AND img_name LIKE '".$topreg['prod_code']."1%'		";
									$getimgResult = mysql_query($getimg, $dbLink);
									$img_name = mysql_fetch_array($getimgResult);
									if(!empty($img_name['img_name'])) {
										echo "<img class='img-responsive' src='prod_images/".$img_name['img_name']."' alt=''>";
									}
									else {
										echo "<img class='img-responsive' src='images/no_image.jpg' alt=''>";
									}
									?>
								</a>
							</div>
							<div class="fashion-grid1">
								<h6 class="best2"><a href="index.php?id=single&pcode=<?php echo $topreg['prod_code'] ?>"><?php echo $topreg['prod_name']; ?></a></h6>
								<span class="price-in1">RM <?php echo $topreg['prod_final_sell_price']; ?></span>
							</div>
							<div class="clearfix"> </div>
						</div>
				<?php
						}
					}
				?>
			</div>
		</div>
		<div class="clearfix"> </div>
		<?php if($max_page > 1) {
			if(!empty($_GET['search'])) $search = "&search=".$_GET['search'];
		?>
		<div align="center">
			<ul class="pagination">
				<li><a href="index.php?id=favourite&pg=1<?php echo $search ?>" <?php if($_GET['pg'] == 1 || empty($_GET['pg'])) echo "class='not-active'"; ?>><i class="fa fa-angle-double-left"></i></a></li>
				<li><a href="<?php if(empty($_GET['pg']) || $_GET['pg'] == 1) echo 'index.php?id=favourite&pg=1'.$search; else echo 'index.php?id=favourite&pg='.($_GET['pg']-1).$search; ?>" <?php if($_GET['pg'] == 1 || empty($_GET['pg'])) echo "class='not-active'"; ?>><i class="fa fa-angle-left"></i></a></li>
				<?php
					for($i=1; $i<=$max_page; $i++) {
						if(empty($_GET['pg']) && $i == 1) {
							echo "<li><a class='active' href='index.php?id=favourite&pg=".$i.$search."'>".$i."</a></li>";
						}
						else if(!empty($_GET['pg']) && $_GET['pg'] == $i) {
							echo "<li><a class='active' href='index.php?id=favourite&pg=".$i.$search."'>".$i."</a></li>";
						}
						else {
							echo "<li><a href='index.php?id=favourite&pg=".$i.$search."'>".$i."</a></li>";
						}
					}
				?>
				<li><a href="<?php if(empty($_GET['pg']) || $_GET['pg'] == 1) echo 'index.php?id=favourite&pg=2'.$search; else echo 'index.php?id=favourite&pg='.($_GET['pg']+1).$search; ?>" <?php if($_GET['pg'] == $max_page) echo "class='not-active'"; ?>><i class="fa fa-angle-right"></i></a></li>
				<li><a href="index.php?id=favourite&pg=<?php echo $max_page ?><?php echo $search ?>" <?php if($_GET['pg'] == $max_page) echo "class='not-active'"; ?>><i class="fa fa-angle-double-right"></i></a></li>
			</ul>
		</div>
		<?php } ?>
		</form>
		<div class="clearfix"> </div>
	</div>
</div>