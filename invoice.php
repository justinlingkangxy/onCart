<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Invoice :: onCart</title>
</head>

<?php
$comres = "SELECT * FROM tbluser WHERE user_email = 'admin@email.com'";
$checkcomres = mysql_query($comres, $dbLink);
if($checkcomres)
	$comreg = mysql_fetch_array($checkcomres);
$res = "SELECT * FROM tblorder, tblship WHERE tblorder.order_num = '".$_GET['oid']."' AND tblorder.order_num = tblship.order_num";
$checkres = mysql_query($res, $dbLink);
if($checkres)
	$reg = mysql_fetch_array($checkres);
?>

<script>
function printContent(id){
	str = document.getElementById(id).innerHTML
	newwin = window.open('','printwin','left=100,top=100,width=1200,height=800')
	newwin.document.write('<HTML>\n<HEAD>\n')
	newwin.document.write('<TITLE>Invoice</TITLE>\n')
	newwin.document.write('<link rel="stylesheet" href="css/bootstrap.css" type="text/css" />\n')
	newwin.document.write('<link rel="stylesheet" href="css/style.css" type="text/css" />\n')
	newwin.document.write('<script>\n')
	newwin.document.write('function chkstate(){\n')
	newwin.document.write('if(document.readyState=="complete"){\n')
	newwin.document.write('window.close()\n')
	newwin.document.write('}\n')
	newwin.document.write('else{\n')
	newwin.document.write('setTimeout("chkstate()",2000)\n')
	newwin.document.write('}\n')
	newwin.document.write('}\n')
	newwin.document.write('function print_win(){\n')
	newwin.document.write('window.print();\n')
	newwin.document.write('chkstate();\n')
	newwin.document.write('}\n')
	newwin.document.write('<\/script>\n')
	newwin.document.write('</HEAD>\n')
	newwin.document.write('<BODY onload="print_win()">\n')
	newwin.document.write(str)
	newwin.document.write('</BODY>\n')
	newwin.document.write('</HTML>\n')
	newwin.document.close()
}
</script>

<div class="products">
	<div class="container">
		<h1>Invoice</h1>
	</div>
</div>
<div id="print_invoice">
	<div class="mation col-md-2"> </div>
	<div class="mation col-md-8" style="border:solid; background:#fff">
		<div align="center">
			<h3 style="color:#333"><u>Invoice <?php echo $reg['order_num'] ?></u></h3>
			<p style="color:#333">Date of Invoice: <?php echo date("Y-m-d", strtotime($reg['order_date_add'])) ?></p>
		</div>
		<br/>
		<div align="center">
		<table style="width:80%">
			<tbody>
				<tr style="background:white">
					<td width="50%"><lebel style="font-size:10pt; color:#888">From</lebel></td>
					<td><lebel style="font-size:10pt; color:#888">To</lebel></td>
				</tr>
				<tr style="background:white">
					<td width="50%"><h1 style="color:#3177b2">onCart</h1></td>
					<td><h1 style="color:#3177b2"><?php echo $reg['ship_name'] ?></h1></td>
				</tr>
				<tr style="background:white">
					<td width="50%"><p style="color:#333"><?php echo $comreg['user_address'] ?></p></td>
					<td><p style="color:#333"><?php echo $reg['ship_address'] ?></p></td>
				</tr>
				<tr style="background:white">
					<td width="50%"><p style="color:#333"><?php echo $comreg['user_postcode']." ".$comreg['user_city'] ?></p></td>
					<td><p style="color:#333"><?php echo $reg['ship_postcode']." ".$reg['ship_city'] ?></p></td>
				</tr>
				<tr style="background:white">
					<td width="50%"><p style="color:#333"><?php echo $comreg['user_state'] ?></p></td>
					<td><p style="color:#333"><?php echo $reg['ship_state'] ?></p></td>
				</tr>
				<tr style="background:white">
					<td width="50%"><p style="color:#333">Tel: <?php echo $comreg['user_mphone'] ?></p></td>
					<td><p style="color:#333">Tel: <?php echo $reg['ship_mphone'] ?></p></td>
				</tr>
			</tbody>
		</table>
		</div>
		<br/>
		<div class="row">
			<div class="col-md-1"></div>
			<div class="col-md-10">
				<table>
					<tr>
						<th>No.</th>
						<th>Item</th>
						<th>Qty</th>		
						<th>Unit Prices</th>
						<th>Product Weight</th>
						<th>Subtotal</th>
					</tr>
					<?php
						if ($checkres) {
							$res = "SELECT * FROM tblorder, tblproduct WHERE tblorder.order_num = '".$_GET['oid']."' AND tblorder.prod_code = tblproduct.prod_code ORDER BY prod_name ASC";
							$checkres = mysql_query($res, $dbLink);
							for($i=0; $i<mysql_num_rows($checkres); $i++) {
								$reg = mysql_fetch_array($checkres);
					?>
								<tr>
									<td><?php echo ($i+1)."."; ?></td>
									<?php if(!empty($reg['prod_gst_code'])) echo "<h6>GST code: ".$reg['prod_gst_code']."</h6>"; ?>
									<td align="center"><?php echo $reg['order_prod_qty']; ?></td>
									<td>RM <?php echo $reg['prod_final_sell_price']; ?></td>
									<td><?php echo $reg['order_prod_weight']; ?> kg</td>
									<td id="sub">RM <?php echo $reg['order_sub_total']; ?></td>
								</tr>
					<?php
								$total += $reg['order_sub_total'];
								$weight += $reg['order_prod_weight'];
							}
							$ship_fee = ($total > 70) ? 0 : (ceil($weight)*15);
							$ship_fee = number_format((float)$ship_fee, 2, '.', '');
							$total = number_format((float)$total, 2, '.', '');
							$grand_total = $total+$ship_fee;
							$grand_total = number_format((float)$grand_total, 2, '.', '');
						}
					?>
					<tr style="border-top-style: solid;"> <td></td> <td></td> <td></td> <td></td> <td align="right">Total: </td> <td>RM <?php echo $total ?></td> </tr>
					<tr> <td></td> <td></td> <td></td> <td></td> <td align="right">Shipping Fees: </td> <td style="border-bottom-style: solid;">RM <?php echo $ship_fee ?></td> </tr>
					<tr> <td></td> <td></td> <td></td> <td></td> <td align="right">Grand Total: </td> <td style="border-width: 5px; border-bottom-style: double;">RM <?php echo $grand_total ?></td> </tr>
				</table>
			</div>
			<div class="col-md-1"></div>
		</div>
	</div>
	<div class="mation col-md-2"> </div>
</div>
<div class='clearfix'> </div>
<div class="container single">
	<button class="btn btn-info btn-1 pull-right" onclick="printContent('print_invoice')">PRINT INVOICE</button>
</div>
<div class='clearfix'> </div>