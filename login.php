<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login :: onCart</title>
</head>

<?php
    if(isset($_POST['btngo'])) {
        $email = $_POST['txtemail'];
        $pass = $_POST['txtpassword'];
        
        if($email == "" or $pass == "") {
            echo "<script>alert('Please enter your E-mail Address or Password'); location='account.php';</script>";
        }
        else {
            $SearchUser = "SELECT * FROM tbluser WHERE user_email = '".$email."' AND user_pass = '".md5($pass)."' AND user_status > 1";
            $SearchUserResult = mysql_query($SearchUser, $dbLink);
            
            if(mysql_num_rows($SearchUserResult) > 0) {
                $RecRow = mysql_fetch_array($SearchUserResult);
                
                if(strtoupper($RecRow['user_level']) == 0) {
                    $_SESSION['level'] = "buyer";
                }
                else if(strtoupper($RecRow['user_level']) == 2) {
                    $_SESSION['level'] = "seller";
                }
                else if(strtoupper($RecRow['user_level']) == 3) {
                    $_SESSION['level'] = "employee";
                }
                else if(strtoupper($RecRow['user_level']) == 4) {
                    $_SESSION['level'] = "admin";
                }
                $_SESSION['email'] = $email;
                $_SESSION['uid'] = $RecRow['user_id'];
                echo "<script>location='index.php';</script>";
            }
            else {
              echo "<script>alert('Incorrect Username or Password'); window.history.back;</script>";
              session_destroy();
            }
        }
    }
?>

<div class="account">
	<div class="container">
		<h1>Account</h1>
		<div class="account_grid">
            <div class="col-md-6 login-right">
            <form id="form_signin" name="form_signin" method="post" action="">
                <span>Email Address</span>
                <input type="email" name="txtemail" required="required"/>
                <span>Password</span>
                <input type="password" name="txtpassword" required="required"/>
                <div class="word-in">
                    <input type="submit" name="btngo" value="Login">
                </div>
            </form>
            </div>	
            <div class="col-md-6 login-left">
                <h4>NEW USER</h4>
                <p>By creating an account with our store, you will be able to view and track your orders in your account and more.</p>
                <a class="acount-btn" href="index.php?id=signup">Create an Account</a>
            </div>
            <div class="clearfix"> </div>
        </div>
	</div>
</div>