<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Terms & Conditions :: onCart</title>
</head>

<?php if($_GET['c'] == '1') { ?>
<div class="container">
	<h1>Making A Purchase</h1>
	<br/>
	<p>
		Making a purchase could not be easier. Just browse our store, and add any items that you wish to buy into the shopping cart. After you have finished your selection, click on 'PROCEED TO PURCHASE' and you will be asked for a few details that we need to be able to complete the order.
	</p>
	<br/>
	<p>
		When confirmation of your order is received, this is to indicate that we have received your order. It does not indicate that a contract exists between us. We will indicate acceptance of your order, and hence a contract between us, when we send you an invoice. We have included this term to protect us in the case that a mistake has been made in pricing, we have inadvertently underpriced goods, or we are no longer able to supply a particular product for some reason. In the case of a change of price, we will always contact you first to ensure that the price is acceptable.
	</p>
</div>
<?php } ?>

<?php if($_GET['c'] == '2') { ?>
<div class="container">
	<h1>Privacy Policy</h1>
	<br/>
	<p>
		onCart do not disclose buyers' information to third parties other than when order details are processed as part of the order fulfilment. In this case, the third party will not disclose any of the details to any other third party.
	</p>
</div>
<?php } ?>

<?php if($_GET['c'] == '3') { ?>
<div class="container">
	<h1>Start Your Business</h1>
	<br/>
	<p>
		If user want to sell products on onCart, user have to register a new account at the register page by choosing the account type as company. Note that the registration may take up to 1 or 2 working days for the administrator or the employee to verify. Please ensure all the information is valid or else onCart has the right to reject seller's registration. onCart will be the mediation platformm to collect the fees and help to ship the products so the buyers should send the products to onCart and the shipping fees should self-cover.
	</p>
	<br/>
	<p>
		onCart will collect the 5% commission fees of the monthly total sales from the seller.
	</p>
</div>
<?php } ?>

<?php if($_GET['c'] == '4') { ?>
<div class="container">
	<h1>Delivery Policy</h1>
	<br/>
	<p>
		The delivery fees is counted by the weight of the parcels. 1 kilogram will cause RM 15 around all the areas in Malaysia. Provide free delivery service if the total purchase fees is more than RM 70.
	</p>
</div>
<?php } ?>

<br/>