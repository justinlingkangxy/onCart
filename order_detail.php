<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Order Details :: onCart</title>
</head>

<?php
if(empty($_GET['mode'])) {
	if(!empty($_GET['sid'])) {
		$res = "SELECT order_num, order_prod_qty, order_sub_total, order_date_add, prod_final_sell_price, order_prod_weight, tblorder.prod_code, prod_name, tblproduct.user_email AS seller_email, tblorder.user_email AS buyer_email, tbluser.user_email AS seller_email FROM tblorder, tblproduct, tbluser WHERE YEAR(order_date_add) = '".$_GET['y']."' AND MONTHNAME(order_date_add) = '".$_GET['m']."' AND DAY(order_date_add) = '".$_GET['d']."' AND tbluser.user_id = '".$_GET['sid']."' AND order_num = '".$_GET['oid']."' AND tbluser.user_email = tblproduct.user_email AND tblproduct.prod_code = tblorder.prod_code AND order_status > 1";
	}
	else {
		$res = "SELECT * FROM tblorder, tblproduct WHERE tblorder.user_email = '".$_SESSION['email']."' AND tblorder.order_num = '".$_GET['oid']."' AND tblorder.prod_code = tblproduct.prod_code ORDER BY prod_name ASC";
	}
}
else {
	$res = "SELECT * FROM tblorder, tblproduct WHERE tblorder.order_num = '".$_GET['oid']."' AND tblorder.prod_code = tblproduct.prod_code ORDER BY prod_name ASC";
}
$checkres = mysql_query($res, $dbLink);
if(!empty($_GET['sid'])) {
	$userres = "SELECT order_date_add, tblorder.user_email AS buyer_email FROM tblorder, tblproduct, tbluser WHERE YEAR(order_date_add) = '".$_GET['y']."' AND MONTHNAME(order_date_add) = '".$_GET['m']."' AND DAY(order_date_add) = '".$_GET['d']."' AND tbluser.user_id = '".$_GET['sid']."' AND order_num = '".$_GET['oid']."' AND tbluser.user_email = tblproduct.user_email AND tblproduct.prod_code = tblorder.prod_code AND order_status > 1";
	$checkuserres = mysql_query($userres, $dbLink);
	$tempreg = mysql_fetch_array($checkuserres);
	$custres = "SELECT user_fullname FROM tbluser WHERE user_email = '".$tempreg['buyer_email']."'";
	$checkcustres = mysql_query($custres, $dbLink);
	$custreg = mysql_fetch_array($checkcustres);
}
else if(!empty($_GET['mode'])) {
	$userres = "SELECT order_date_add, tblorder.user_email AS buyer_email FROM tblorder, tblproduct, tbluser WHERE YEAR(order_date_add) = '".$_GET['y']."' AND MONTHNAME(order_date_add) = '".$_GET['m']."' AND DAY(order_date_add) = '".$_GET['d']."' AND order_num = '".$_GET['oid']."' AND tblproduct.prod_code = tblorder.prod_code AND order_status > 1";
	$checkuserres = mysql_query($userres, $dbLink);
	$tempreg = mysql_fetch_array($checkuserres);
	$custres = "SELECT user_fullname FROM tbluser WHERE user_email = '".$tempreg['buyer_email']."'";
	$checkcustres = mysql_query($custres, $dbLink);
	$custreg = mysql_fetch_array($checkcustres);
}
?>

<div class="mation col-md-2"> </div>
<div class="mation col-md-8">
	<div class="products">
		<div class="container">
			<h1>Order Details<br/>#<?php echo $_GET['oid'] ?></h1>
		</div>
	</div>
	<?php
	if(!empty($_GET['sid']) || !empty($_GET['mode'])) {
		$newdateformat = strtotime($tempreg['order_date_add']);
		$newdateformat = date('Y-m-d', $newdateformat);
	?>
		<div class="row">
			<p title="<?php echo $tempreg['buyer_email'] ?>">Customer: <?php echo $custreg['user_fullname'] ?></p>
			<p>Order date: <?php echo $newdateformat ?></p>
		</div>
		<br/>
	<?php } ?>
	<table>
		<tr>
			<th>No.</th>
			<th>Item</th>
			<th>Quantities</th>		
			<th>Unit Prices</th>
			<th>Product Weight</th>
			<th>Subtotal</th>
		</tr>
		<?php
			if ($checkres) {
				for($i=0; $i<mysql_num_rows($checkres); $i++) {
					$reg = mysql_fetch_array($checkres);
		?>
					<tr>
						<td><?php echo ($i+1)."."; ?></td>
						<td class="ring-in" style="cursor:pointer" onclick="location='index.php?id=single&pcode=<?php echo $reg['prod_code']; ?>'"><a href="#" class="at-in">
							<?php
							$getimg = "SELECT img_name FROM tblimage WHERE img_code = '".$reg['prod_code']."' AND img_name LIKE '".$reg['prod_code']."1%'";
							$getimgResult = mysql_query($getimg, $dbLink);
							$img_name = mysql_fetch_array($getimgResult);
							if(!empty($img_name['img_name'])) {
								echo "<img class='img-responsive' src='prod_images/".$img_name['img_name']."' width='auto' height='100px' alt=''>";
							}
							else {
								echo "<img class='img-responsive' src='images/no_image.jpg' width='auto' height='100px' alt=''>";
							}
							?>
							</a>
							<div class="sed">
								<h5><?php echo $reg['prod_name']; ?></h5>
								<p><?php echo $reg['prod_desc']; ?></p>
							</div>
							<div class="clearfix"> </div>
						</td>
						<td align="center"><?php echo $reg['order_prod_qty']; ?></td>
						<td>RM <?php echo $reg['prod_final_sell_price']; ?></td>
						<td><?php echo $reg['order_prod_weight']; ?> kg</td>
						<td id="sub">RM <?php echo $reg['order_sub_total']; ?></td>
					</tr>
		<?php
					$total += $reg['order_sub_total'];
					$weight += $reg['order_prod_weight'];
				}
				$ship_fee = ($total > 70) ? 0 : (ceil($weight)*15);
				$ship_fee = number_format((float)$ship_fee, 2, '.', '');
				$total = number_format((float)$total, 2, '.', '');
				$grand_total = $total+$ship_fee;
				$grand_total = number_format((float)$grand_total, 2, '.', '');
			}
			if(!empty($_GET['sid'])) {
		?>
			<tr style="border-top-style: solid;"> <td></td> <td></td> <td></td> <td></td> <td align="right">Total: </td> <td style="border-width: 5px; border-bottom-style: double;">RM <?php echo $total ?></td> </tr>
		<?php } else { ?>
			<tr style="border-top-style: solid;"> <td></td> <td></td> <td></td> <td></td> <td align="right">Total: </td> <td>RM <?php echo $total ?></td> </tr>
			<tr> <td></td> <td></td> <td></td> <td></td> <td align="right">Shipping Fees: </td> <td style="border-bottom-style: solid;">RM <?php echo $ship_fee ?></td> </tr>
			<tr> <td></td> <td></td> <td></td> <td></td> <td align="right">Grand Total: </td> <td style="border-width: 5px; border-bottom-style: double;">RM <?php echo $grand_total ?></td> </tr>
		<?php } ?>
	</table>
	<div class='clearfix'> </div>
	<?php if(empty($_GET['sid'])) { ?>
		<a class="btn btn-info btn-1" href="index.php?id=invoice&oid=<?php echo $_GET['oid']; ?>">View Invoice</a>
	<?php } ?>
</div>
<div class="mation col-md-2"> </div>
<div class='clearfix'> </div>