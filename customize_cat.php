<?php session_start(); ?>
<?php if(!empty($_SESSION['email']) && $_SESSION['level'] == "admin") { ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Customize Category :: onCart</title>
</head>

<?php if($_GET['mode'] == "list") { ?>
	<?php
	if(isset($_POST['btnsearch'])) {
		$attr = $_SERVER['QUERY_STRING'];
		if(!empty($_GET['pg'])) {
			$arr = explode('&pg=', $attr);
			$attr = $arr[0];
		}
		else if(!empty($_GET['search'])) {
			$arr = explode('&search=', $attr);
			$attr = $arr[0];
		}
		echo "<script>location='index.php?".$attr."&search=".$_POST['txtsearch']."';</script>";
	}
	?>

	<div align="center">
	<div class="products">
		<div class="container">
			<h1>Customize Category</h1>
		</div>
	</div>
	<form action="" method="post" accept-charset="utf-8">
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-8">
					<div class="pull-right"><input type="text" name="txtsearch" value="" placeholder="Category or Sub Category"> <input type="submit" name="btnsearch" value="Search" class="btn btn-info btn-sm"></div> <br/>
			</div>
			<div class="col-md-2"></div>
		</div>
		<br/>
		<table class="table">
			<thead>
				<tr>
					<th>No.</th>
					<th>Category Name</th>
					<th>Sub Category Name</th>
					<th>Action</th>
				</tr>
			</thead>
			<tbody id="table_content">
				<?php
					if($_GET['pg'] == "") {
						$page = 0;
					}
					else {
						$page = ($_GET['pg']*10)-10;
					}
					$res = "SELECT * FROM tblcategory";
					if(!empty($_GET['search'])) {
						$res .= " WHERE (cat_name LIKE '%".$_GET['search']."%' OR cat_subname LIKE '%".$_GET['search']."%')";
					}
					$res .= " ORDER BY cat_name ASC, cat_subname ASC";
					$checkres = mysql_query($res, $dbLink);
					$num = mysql_num_rows($checkres);
					$max_page = ceil($num/10);
					$res .= " LIMIT ".$page.",10";
					$checkres = mysql_query($res, $dbLink);
					if(mysql_num_rows($checkres) > 0) {
						for($i=0; $i<mysql_num_rows($checkres); $i++) {
							$reg = mysql_fetch_array($checkres);
				?>
							<tr>
								<td><?php echo ($i+1)."."; ?></td>
								<td><?php echo $reg['cat_name'];; ?></td>
								<td><?php echo $reg['cat_subname']; ?></td>
								<td>
									<a class="btn btn-info btn-xs" href="index.php?id=customize_cat&mode=edit&cid=<?php echo $reg['cat_id']; ?>" title="Edit"><i class="fa fa-pencil-square-o"></i></a>
									<button class="btn btn-danger btn-xs" title="Remove" name="btnremove" onclick="return confirm('Are you sure want to remove this category?')" value="<?php echo $reg['cat_id']; ?>"><i class="fa fa-times"></i></button>
								</td>
							</tr>
				<?php 	}
					}
					else {
						echo "<tr><td></td><td></td><td>No Record(s) Found.</td><td></td></tr>";
					}
				?>
			</tbody>
		</table>
		</div> <div class='clearfix'> </div>
		<?php if($max_page > 1) {
			if(!empty($_GET['search'])) $search = "&search=".$_GET['search'];
			$mode = "&mode=".$_GET['mode'];
		?>
		<div align="center">
			<ul class="pagination">
				<li><a href="index.php?id=customize_cat<?php echo $mode; ?>&pg=1<?php echo $search; ?>" <?php if($_GET['pg'] == 1 || empty($_GET['pg'])) echo "class='not-active'"; ?>><i class="fa fa-angle-double-left"></i></a></li>
				<li><a href="<?php if(empty($_GET['pg']) || $_GET['pg'] == 1) echo 'index.php?id=customize_cat'.$mode.'&pg=1'.$search; else echo 'index.php?id=customize_cat'.$mode.'&pg='.($_GET['pg']-1).$search; ?>" <?php if($_GET['pg'] == 1 || empty($_GET['pg'])) echo "class='not-active'"; ?>><i class="fa fa-angle-left"></i></a></li>
				<?php
					for($i=1; $i<=$max_page; $i++) {
						if(empty($_GET['pg']) && $i == 1) {
							echo "<li><a class='active' href='index.php?id=customize_cat".$mode."&pg=".$i.$search."'>".$i."</a></li>";
						}
						else if(!empty($_GET['pg']) && $_GET['pg'] == $i) {
							echo "<li><a class='active' href='index.php?id=customize_cat".$mode."&pg=".$i.$search."'>".$i."</a></li>";
						}
						else {
							echo "<li><a href='index.php?id=customize_cat".$mode."&pg=".$i.$search."'>".$i."</a></li>";
						}
					}
				?>
				<li><a href="<?php if(empty($_GET['pg']) || $_GET['pg'] == 1) echo 'index.php?id=customize_cat'.$mode.'&pg=2'.$search; else echo 'index.php?id=customize_cat'.$mode.'&pg='.($_GET['pg']+1).$search; ?>" <?php if($_GET['pg'] == $max_page) echo "class='not-active'"; ?>><i class="fa fa-angle-right"></i></a></li>
				<li><a href="index.php?id=customize_cat<?php echo $mode; ?>&pg=<?php echo $max_page; ?><?php echo $search; ?>" <?php if($_GET['pg'] == $max_page) echo "class='not-active'"; ?>><i class="fa fa-angle-double-right"></i></a></li>
			</ul>
		</div>
		<?php } ?>
	</form>
	</div>

	<?php
		if(isset($_POST['btnremove'])) {
			$updatecat = "DELETE FROM tblcategory WHERE cat_id = '".$_POST['btnremove']."'";
			$updatecatres = mysql_query($updatecat, $dbLink);
			if($updatecatres) 
				echo "<script>alert('Remove Category Successfully!'); location='index.php?id=customize_cat&mode=list';</script>";
			else
				echo "<script>alert('Remove Category Failed!');</script>";
		}
	?>
<?php } else if($_GET['mode'] == "add" || $_GET['mode'] == "edit") { ?>
<div class="container">
	<div class="register">
		<h1><?php if(!empty($_GET['mode']) && $_GET['mode'] == "edit") echo "Edit"; else echo "Add"; ?> Category</h1>
		<form id="form_customize_cat" name="form_customize_cat" method="post" action="" enctype="multipart/form-data">
			<div class="col-md-8 register-top-grid">
				<div class="mation">
					<div class="col-md-6">
						<span><?php if($_GET['mode'] == "add") echo "New "; ?>Category</span>
						<select class="form-group-lg form-control" style="margin-top: 12px; margin-bottom: 12px" name="sel_cat" id="sel_cat" onchange="fetch_select(this.value);">
							<option value="">Please select...</option>
							<?php
								$catres = "SELECT cat_id, cat_name FROM tblcategory GROUP BY cat_name";
								$checkcatres = mysql_query($catres, $dbLink);
								while($row = mysql_fetch_array($checkcatres))
								{
									$getcatres = "SELECT cat_name FROM tblcategory WHERE cat_id = '".$_GET['cid']."' GROUP BY cat_name";
									$checkgetcatres = mysql_query($getcatres, $dbLink);
									$getcat = mysql_fetch_array($checkgetcatres);
									if($row['cat_name'] == $getcat['cat_name']) {
										echo "<option selected='selected' value='".$row['cat_name']."'>".$row['cat_name']."</option>";
									}
									else
										echo "<option value='".$row['cat_name']."'>".$row['cat_name']."</option>";
								}
							?>
							<option id="new_cat" value="new">New...</option>
						</select>
					</div>

					<div class="col-md-6" id="newcat" style="display: none" >
						<span id="text_get">New Category Name</span>
						<input type="text" name="txtcat" id="txtcat" />
					</div>
					<?php
						$getsubcatres = "SELECT cat_subname FROM tblcategory WHERE cat_id = '".$_GET['cid']."'";
						$checkgetsubcatres = mysql_query($getsubcatres, $dbLink);
						$subcat = mysql_fetch_array($checkgetsubcatres);
					?>
					<div class="col-md-6" >
						<span>New Sub Category Name</span>
						<input type="text" name="txtsubcat" id="txtsubcat" <?php if($_GET['mode'] == "add") echo "required='required'"; ?> value="<?php echo $subcat['cat_subname']; ?>" />
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="clearfix"> </div>
				
			<div class="register-but">
				<input type="submit" <?php if($_GET['mode'] == 'edit') {echo "value='Update' name='btnupdate'";} else {echo "value='Add' name='btnadd'";} ?> >
				<div class="clearfix"> </div>
			</div>
		</form>
	</div>
</div>

<?php
if(isset($_POST['btnupdate'])) {
	if($_POST['sel_cat'] != "") {
		$cat_name = ($_POST['sel_cat'] == "new") ? $_POST['txtcat'] : $_POST['sel_cat'] ;
		$cat_subname = $_POST['txtsubcat'];
		$sqlcat = "UPDATE tblcategory SET cat_name = '".ucfirst(strtolower($cat_name))."', cat_subname = '".ucfirst(strtolower($cat_subname))."' WHERE cat_id = '".$_GET['cid']."'";
		$sqlcatres = mysql_query($sqlcat, $dbLink);
		if($sqlcatres) {
				echo "<script>alert('Category update successfull.'); window.history.back();</script>";
		}
		else {
			echo "<script>alert('Category update failed!'); window.history.back();</script>";
		}
	}
	else {
		echo "<script>alert('Please select category!'); window.history.back();</script>";
	}
}

if(isset($_POST['btnadd'])) {
	if($_POST['sel_cat'] != "") {
		$cat_name = ($_POST['sel_cat'] == "new") ? $_POST['txtcat'] : $_POST['sel_cat'] ;
		$cat_subname = $_POST['txtsubcat'];
		$sqlcat = "INSERT INTO tblcategory(cat_name, cat_subname) VALUES ('".ucfirst(strtolower(trim($cat_name)))."', '".ucfirst(strtolower(trim($cat_subname)))."')";
		$sqlcatres = mysql_query($sqlcat, $dbLink);
		if($sqlcatres) {
				echo "<script>alert('New category added successfull.'); window.history.back();</script>";
		}
		else {
			echo "<script>alert('New category added failed!'); window.history.back();</script>";
		}
	}
	else {
		echo "<script>alert('Please select category!'); window.history.back();</script>";
	}
}
?>

<script type="text/javascript">
function fetch_select(val)
{
	$.ajax({
		type: 'post',
		url: 'fetch_cat_data.php',
		data: {
			get_option:val
		},
		success: function (response) {
			document.getElementById("sel_subcat").innerHTML=response; 
		}
	});
}
</script>

<script type="text/javascript"> //used to show new category
	$(function() {
		$('#sel_cat').click(function () {
			if ($("#new_cat").is(":selected")) {
				$("#txtcat").attr("required", "required");
				$("#newcat").show();
			}
			else {
				$("#txtcat").removeAttr("required");
				$("#newcat").hide();
			}
		});
	});
</script>
<?php } ?>
<?php
}
else {
	echo "<script>alert('Access Denied'); location='index.php';</script>";
}
?>