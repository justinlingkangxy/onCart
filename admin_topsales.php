<?php session_start(); ?>
<?php if(!empty($_SESSION['email']) && ($_SESSION['level'] == "admin" || $_SESSION['level'] == "employee")) { ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php echo $_GET['y']; ?> Seller's Top Sales Report :: onCart</title>
</head>

<?php
if($_GET['pg'] == "") {
	$page = 0;
}
else {
	$page = ($_GET['pg']*10)-10;
}
$res = "SELECT tbluser.user_email, tbluser.user_fullname, tbluser.user_id FROM tblorder, tblproduct, tbluser WHERE tblorder.order_status > 1 AND tblorder.prod_code = tblproduct.prod_code AND tblproduct.user_email = tbluser.user_email AND tbluser.user_email != 'admin@email.com' AND YEAR(order_date_add) = '".$_GET['y']."' GROUP BY tbluser.user_email";
$checkadminres = mysql_query($res, $dbLink);
$num = mysql_num_rows($checkadminres);
$max_page = ceil($num/10);
$res .= " LIMIT ".$page.",10";
$checkres = mysql_query($res, $dbLink);
?>

<div align="center">
<div class="products">
	<div class="container">
		<h1><?php echo $_GET['y']; ?> Seller's Top Sales Report</h1>
	</div>
</div>
<form action="" method="post" accept-charset="utf-8">
	<div class="col-md-6"></div>
	<div class="col-md-6">
		<select class="form-group-lg form-control" style="width: auto;" name="sel_year" onchange="location='index.php?id=admin_sales_report&y=' + this.value;">
			<?php
			$yearres = "SELECT YEAR(order_date_add) AS year FROM tblorder WHERE order_status > 1 GROUP BY YEAR(order_date_add)";
			$checkyearres = mysql_query($yearres, $dbLink);
			for($i=0; $i<mysql_num_rows($checkyearres); $i++) {
				$yearreg = mysql_fetch_array($checkyearres);
				if($yearreg['year'] == $_GET['y']) {
					echo "<option selected='selected' value='".$yearreg['year']."'>".$yearreg['year']."</option>";
				}
				else {
					echo "<option value='".$yearreg['year']."'>".$yearreg['year']."</option>";
				}
			}
			?>
		</select>
	</div>
	<br/><br/>
	<table class="table">
		<thead>
			<tr>
				<th>No.</th>
				<th>Seller Name</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody id="table_content">
			<?php
				if(mysql_num_rows($checkres) > 0) {
					for($i=0; $i<mysql_num_rows($checkres); $i++) {
						$reg = mysql_fetch_array($checkres);
			?>
						<tr>
							<td><?php echo ($i+1); ?></td>
							<td><span title="<?php echo $reg['user_email']; ?>"><?php echo $reg['user_fullname']; ?></span></td>
							<td>
								<a class="btn btn-info btn-xs" href="index.php?id=topsales&uid=<?php echo $reg['user_id']; ?>&dtype=m&y=<?php echo $_GET['y']; ?>" title="View Details"><i class="fa fa-list-alt"></i></a>
							</td>
						</tr>
			<?php
					}
				}
				else {
					echo "<tr><td></td><td></td><td>No Record(s) Found.</td><td></td></tr>";
				}
			?>
		</tbody>
	</table>
	</div> <div class='clearfix'> </div>
	<?php if($max_page > 1) { ?>
	<div align="center">
		<ul class="pagination">
			<li><a href="index.php?id=admin_topsales&y=<?php echo $_GET['y']; ?>&pg=1" <?php if($_GET['pg'] == 1 || empty($_GET['pg'])) echo "class='not-active'"; ?>><i class="fa fa-angle-double-left"></i></a></li>
			<li><a href="<?php if(empty($_GET['pg']) || $_GET['pg'] == 1) echo 'index.php?id=admin_topsales&y='.$_GET['y'].'&pg=1'; else echo 'index.php?id=admin_topsales&y='.$_GET['y'].'&pg='.($_GET['pg']-1); ?>" <?php if($_GET['pg'] == 1 || empty($_GET['pg'])) echo "class='not-active'"; ?>><i class="fa fa-angle-left"></i></a></li>
			<?php
				for($i=1; $i<=$max_page; $i++) {
					if(empty($_GET['pg']) && $i == 1) {
						echo "<li><a class='active' href='index.php?id=admin_topsales&y=".$_GET['y']."&pg=".$i."'>".$i."</a></li>";
					}
					else if(!empty($_GET['pg']) && $_GET['pg'] == $i) {
						echo "<li><a class='active' href='index.php?id=admin_topsales&y=".$_GET['y']."&pg=".$i."'>".$i."</a></li>";
					}
					else {
						echo "<li><a href='index.php?id=admin_topsales&y=".$_GET['y']."&pg=".$i."'>".$i."</a></li>";
					}
				}
			?>
			<li><a href="<?php if(empty($_GET['pg']) || $_GET['pg'] == 1) echo 'index.php?id=admin_topsales&y='.$_GET['y'].'&pg=2'; else echo 'index.php?id=admin_topsales&y='.$_GET['y'].'&pg='.($_GET['pg']+1); ?>" <?php if($_GET['pg'] == $max_page) echo "class='not-active'"; ?>><i class="fa fa-angle-right"></i></a></li>
			<li><a href="index.php?id=admin_topsales&y=<?php echo $_GET['y']; ?>&pg=<?php echo $max_page ?>" <?php if($_GET['pg'] == $max_page) echo "class='not-active'"; ?>><i class="fa fa-angle-double-right"></i></a></li>
		</ul>
	</div>
	<?php } ?>
</form>
</div>

<?php
}
else {
	echo "<script>alert('Access Denied'); location='index.php';</script>";
}
?>