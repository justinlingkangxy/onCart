<?php session_start(); ?>
<?php if(!empty($_SESSION['email']) && ($_SESSION['level'] == "admin" || $_SESSION['level'] == "seller")) { ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>List Product :: onCart</title>
</head>

<?php
if(isset($_POST['btnsearch'])) {
	$attr = $_SERVER['QUERY_STRING'];
	if(!empty($_GET['pg'])) {
		$arr = explode('&pg=', $attr);
		$attr = $arr[0];
	}
	else if(!empty($_GET['search'])) {
		$arr = explode('&search=', $attr);
		$attr = $arr[0];
	}
	echo "<script>location='index.php?".$attr."&search=".$_POST['txtsearch']."';</script>";
}
?>

<?php
if($_GET['pg'] == "") {
	$page = 0;
}
else {
	$page = ($_GET['pg']*10)-10;
}
$res = "SELECT * FROM tblproduct WHERE user_email = '".$_SESSION['email']."'";
if(!empty($_GET['search'])) {
	$res .= " AND prod_name LIKE '%".$_GET['search']."%'";
}
$res .= " ORDER BY CASE WHEN prod_qty != '-1' AND prod_qty < 10 THEN 0 ELSE 1 END, prod_date_add ASC";
$checkres = mysql_query($res, $dbLink);
$num = mysql_num_rows($checkres);
$max_page = ceil($num/10);
$res .= " LIMIT ".$page.",10";
$checkres = mysql_query($res, $dbLink);
?>

<div align="center">
<div class="products">
	<div class="container">
		<h1>List Product</h1>
	</div>
</div>
<form action="" method="post" accept-charset="utf-8">
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
				<div class="pull-right"><input type="text" name="txtsearch" value="" placeholder="Product Name"> <input type="submit" name="btnsearch" value="Search" class="btn btn-info btn-sm"></div> <br/>
		</div>
		<div class="col-md-2"></div>
	</div>
	<br/>
	<table class="table">
		<thead>
			<tr>
				<th>No.</th>
				<th>Date Added</th>
				<th>Product Name</th>
				<th>GST</th>
				<th>Selling Price</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody id="table_content">
			<?php
				if(mysql_num_rows($checkres) > 0) {
					for($i=0; $i<mysql_num_rows($checkres); $i++) {
						$reg = mysql_fetch_array($checkres);
						$date_time = explode(' ', $reg['prod_date_add']);
						$date = $date_time[0];
						$time = $date_time[1];
			?>
						<tr>
							<td><?php echo ($i+1)."."; ?>&nbsp;<?php if($reg['prod_qty'] <= 10 && $reg['prod_qty'] != '-1') echo "<span class='badge' title='Lack of Stock'>!</span>"; ?></td>
							<td><span title="<?php echo $time; ?>"><?php echo $date; ?></span></td>
							<td><?php echo $reg['prod_name']; ?></td>
							<td><?php if($reg['prod_gst'] == "1.06") echo "<span class='label label-success'><i class='fa fa-check'></i></span>"; else echo "<span class='label label-danger'><i class='fa fa-times'></i></span>"; ?></td>
							<td>RM <?php echo $reg['prod_final_sell_price']; ?></td>
							<td>
								<a class="btn btn-info btn-xs" href="index.php?id=edit_prod&mode=edit&pcode=<?php echo $reg['prod_code']; ?>" title="View Details"><i class="fa fa-list-alt"></i></a>
							</td>
						</tr>
			<?php 	}
				}
				else {
					echo "<tr><td></td><td></td><td>No Record(s) Found.</td><td></td><td></td></tr>";
				}
			?>
		</tbody>
	</table>
	</div> <div class='clearfix'> </div>
	<?php if($max_page > 1) {
		if(!empty($_GET['search'])) $search = "&search=".$_GET['search'];
	?>
	<div align="center">
		<ul class="pagination">
			<li><a href="index.php?id=list_prod&pg=1<?php echo $search ?>" <?php if($_GET['pg'] == 1 || empty($_GET['pg'])) echo "class='not-active'"; ?>><i class="fa fa-angle-double-left"></i></a></li>
			<li><a href="<?php if(empty($_GET['pg']) || $_GET['pg'] == 1) echo 'index.php?id=list_prod&pg=1'.$search; else echo 'index.php?id=list_prod&pg='.($_GET['pg']-1).$search; ?>" <?php if($_GET['pg'] == 1 || empty($_GET['pg'])) echo "class='not-active'"; ?>><i class="fa fa-angle-left"></i></a></li>
			<?php
				for($i=1; $i<=$max_page; $i++) {
					if(empty($_GET['pg']) && $i == 1) {
						echo "<li><a class='active' href='index.php?id=list_prod&pg=".$i.$search."'>".$i."</a></li>";
					}
					else if(!empty($_GET['pg']) && $_GET['pg'] == $i) {
						echo "<li><a class='active' href='index.php?id=list_prod&pg=".$i.$search."'>".$i."</a></li>";
					}
					else {
						echo "<li><a href='index.php?id=list_prod&pg=".$i.$search."'>".$i."</a></li>";
					}
				}
			?>
			<li><a href="<?php if(empty($_GET['pg']) || $_GET['pg'] == 1) echo 'index.php?id=list_prod&pg=2'.$search; else echo 'index.php?id=list_prod&pg='.($_GET['pg']+1).$search; ?>" <?php if($_GET['pg'] == $max_page) echo "class='not-active'"; ?>><i class="fa fa-angle-right"></i></a></li>
			<li><a href="index.php?id=list_prod&pg=<?php echo $max_page ?><?php echo $search ?>" <?php if($_GET['pg'] == $max_page) echo "class='not-active'"; ?>><i class="fa fa-angle-double-right"></i></a></li>
		</ul>
	</div>
	<?php } ?>
</form>
</div>

<?php
}
else {
	echo "<script>alert('Access Denied'); location='index.php';</script>";
}
?>