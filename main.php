<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Main :: onCart</title>
</head>

<div class="banner">
	<div class="col-sm-2 banner-mat"></div>
	<div class="col-sm-8 matter-banner">
		<div class="slider">
			<div class="callbacks_container">
				<ul class="rslides" id="slider">
					<li><img src="images/1.jpg" alt=""></li>
					<li><img src="images/2.jpg" alt=""></li>
					<li><img src="images/3.jpg" alt=""></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="col-sm-2 banner-mat"> </div>
	<div class="clearfix"></div>
</div>

<?php
if(isset($_POST['btnaddcart'])) {
	if(empty($_SESSION['email'])) {
		echo "<script>alert('Please login.'); location='index.php?id=login';</script>";
	}
	else {
		$countcart = "SELECT COUNT(cart_id) AS RecordNum FROM tblcart WHERE prod_code = '".$_POST['btnaddcart']."' AND user_email = '".$_SESSION['email']."'";
		$countcartres = mysql_query($countcart, $dbLink);
		$Row = mysql_fetch_array($countcartres);
		if($Row['RecordNum'] > 0) {
			$checkQuery = "SELECT * FROM tblcart, tblproduct WHERE tblcart.prod_code = '".$_POST['btnaddcart']."' AND tblcart.prod_code = tblproduct.prod_code AND tblcart.user_email = '".$_SESSION['email']."'";
			$checkQueryRes = mysql_query($checkQuery, $dbLink);
			if(mysql_num_rows($checkQueryRes) > 0) {
				$reg = mysql_fetch_array($checkQueryRes);
				$new_qty = ($reg['cart_prod_qty']+1);
				$sub_total = $reg['prod_final_sell_price']*$new_qty;
				$prod_weight = $reg['prod_weight']*$new_qty;
				$updcart = "UPDATE tblcart SET cart_prod_qty = '".$new_qty."', cart_prod_weight = '".$prod_weight."', cart_sub_total = '".$sub_total."', cart_date_upd = '".date("Y-m-d H:i:s")."' WHERE prod_code = '".$reg['prod_code']."'";
				$updcartresult = mysql_query($updcart, $dbLink);
				echo "<script>location='index.php?".$_SERVER['QUERY_STRING']."';</script>";
			}
		}
		else {
			$checkQuery = "SELECT prod_final_sell_price, prod_weight FROM tblproduct WHERE prod_code = '".$_POST['btnaddcart']."'";
			$checkQueryRes = mysql_query($checkQuery, $dbLink);
			$reg = mysql_fetch_array($checkQueryRes);
			$sqlCart = "INSERT INTO tblcart(prod_code, user_email, cart_prod_qty, cart_prod_weight, cart_sub_total, cart_date_add) VALUES('".$_POST['btnaddcart']."', '".$_SESSION['email']."', '1', '".$reg['prod_weight']."', '".$reg['prod_final_sell_price']."', '".date("Y-m-d H:i:s")."')";
			$sqlCartResult = mysql_query($sqlCart, $dbLink);
			echo "<script>location='index.php?".$_SERVER['QUERY_STRING']."';</script>";
		}
	}
}
if(isset($_POST['btnwish'])) {
	if(empty($_SESSION['email'])) {
		echo "<script>alert('Please login.'); location='index.php?id=login';</script>";
	}
	else {
		$inwishres = "INSERT INTO tblwish(prod_code, user_email, wish_date_add) VALUES('".$_POST['btnwish']."', '".$_SESSION['email']."', '".date("Y-m-d H:i:s")."')";
		$checkinwishres = mysql_query($inwishres, $dbLink);
		echo "<script>alert('Item added into wish list.'); location='index.php?".$_SERVER['QUERY_STRING']."';</script>";
	}
}
if(isset($_POST['btnsearch'])) {
	$attr = $_SERVER['QUERY_STRING'];
	if(!empty($_GET['pg'])) {
		$arr = explode('&pg=', $attr);
		$attr = $arr[0];
	}
	else if(!empty($_GET['search'])) {
		$arr = explode('&search=', $attr);
		$attr = $arr[0];
	}
	echo "<script>location='index.php?search=".$_POST['txtsearch']."';</script>";
}
?>

<div class="content">
	<div class="container">
		<div class="content-top">
			<h1>Recent Products</h1>
			<form action="" method="post" name="form_prod_add" id="form_prod_add">
				<div class="pull-right"><input type="text" name="txtsearch" value="" placeholder="Product Name"> <input type="submit" name="btnsearch" value="Search" class="btn btn-info btn-sm"></div> <br/>
				<?php
					if($_GET['pg'] == "") {
						$page = 0;
					}
					else {
						$page = ($_GET['pg']*8)-8;
					}
					$res = "SELECT * FROM tblproduct WHERE (prod_qty > 0 OR prod_qty = '-1')";
					if(!empty($_GET['search'])) {
						$res .= " AND prod_name LIKE '%".$_GET['search']."%'";
					}
					$res .= " ORDER BY prod_date_add DESC";
					$max_page = 3;
					$res .= " LIMIT ".$page.",8";
					$checkres = mysql_query($res, $dbLink);
					if(mysql_num_rows($checkres) > 0) {
				?>
						<div class="content-top1">
				<?php
						for($i=0; $i<mysql_num_rows($checkres); $i++) {
							$reg = mysql_fetch_array($checkres);
				?>
							<div class="col-md-3 col-md2">
								<div class="col-md1 simpleCart_shelfItem">
									<a href="index.php?id=single&pcode=<?php echo $reg['prod_code'] ?>">
										<?php
										$getimg = "SELECT img_name FROM tblimage WHERE img_code = '".$reg['prod_code']."' AND img_name LIKE '".$reg['prod_code']."1%'";
										$getimgResult = mysql_query($getimg, $dbLink);
										$img_name = mysql_fetch_array($getimgResult);
										if(!empty($img_name['img_name'])) {
											echo "<img class='img-product' src='prod_images/".$img_name['img_name']."' alt=''>";
										}
										else {
											echo "<img class='img-product' src='images/no_image.jpg' alt=''>";
										}
										?>
									</a>
									<h3><a href="index.php?id=single&pcode=<?php echo $reg['prod_code'] ?>"><?php echo $reg['prod_name']; ?></a></h3>
									<h2 class="item_price pull-left" style="font-family: Arial;">RM <?php echo $reg['prod_final_sell_price']; ?></h2>
									<button type="submit" name="btnaddcart" title="Add to Cart" class="btn btn-xs btn-info pull-right" value="<?php echo $reg['prod_code']; ?>"><i class="fa fa-shopping-cart"></i></button>
									<?php
										$wishres = "SELECT prod_code FROM tblwish WHERE prod_code = '".$reg['prod_code']."' AND user_email = '".$_SESSION['email']."'";
										$checkwishres = mysql_query($wishres, $dbLink);
										$wishreg = mysql_fetch_array($checkwishres);
										if(mysql_num_rows($checkwishres) == 0) {
									?>
										<button type="submit" name="btnwish" title="Add to Favourite" class="btn btn-xs btn-danger pull-right" value="<?php echo $reg['prod_code']; ?>"><i class="fa fa-heart"></i></button>
									<?php } ?>
									<div class="clearfix"> </div>
								</div>
							</div>
				<?php
							if(($i+1) % 4 == 0)
								echo "<div class='clearfix'> </div> </div> <div class='content-top1'>";
						}
						echo "</div> <div class='clearfix'> </div>";
				?>
						<?php if($max_page > 1) {
							if(!empty($_GET['search'])) $search = "&search=".$_GET['search'];
						?>
						<div align="center">
							<ul class="pagination">
								<li><a href="index.php?&pg=1<?php echo $search ?>" <?php if($_GET['pg'] == 1 || empty($_GET['pg'])) echo "class='not-active'"; ?>><i class="fa fa-angle-double-left"></i></a></li>
								<li><a href="<?php if(empty($_GET['pg']) || $_GET['pg'] == 1) echo 'index.php?&pg=1'.$search; else echo 'index.php?&pg='.($_GET['pg']-1).$search; ?>" <?php if($_GET['pg'] == 1 || empty($_GET['pg'])) echo "class='not-active'"; ?>><i class="fa fa-angle-left"></i></a></li>
								<?php
									for($i=1; $i<=$max_page; $i++) {
										if(empty($_GET['pg']) && $i == 1) {
											echo "<li><a class='active' href='index.php?&pg=".$i.$search."'>".$i."</a></li>";
										}
										else if(!empty($_GET['pg']) && $_GET['pg'] == $i) {
											echo "<li><a class='active' href='index.php?&pg=".$i.$search."'>".$i."</a></li>";
										}
										else {
											echo "<li><a href='index.php?&pg=".$i.$search."'>".$i."</a></li>";
										}
									}
								?>
								<li><a href="<?php if(empty($_GET['pg']) || $_GET['pg'] == 1) echo 'index.php?&pg=2'.$search; else echo 'index.php?&pg='.($_GET['pg']+1).$search; ?>" <?php if($_GET['pg'] == $max_page) echo "class='not-active'"; ?>><i class="fa fa-angle-right"></i></a></li>
								<li><a href="index.php?&pg=<?php echo $max_page ?><?php echo $search ?>" <?php if($_GET['pg'] == $max_page) echo "class='not-active'"; ?>><i class="fa fa-angle-double-right"></i></a></li>
							</ul>
						</div>
						<?php } ?>
				<?php
					} else {
						echo "<br/><h3 class='h_text_style' align='center'>No Product(s) Found.</h3>";
					}
				?>
			</form>
		</div>
	</div>
</div>