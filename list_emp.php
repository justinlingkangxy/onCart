<?php session_start(); ?>
<?php if(!empty($_SESSION['email']) && $_SESSION['level'] == "admin") { ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>List Emplyee :: onCart</title>
</head>

<?php
if(isset($_POST['btnsearch'])) {
	$attr = $_SERVER['QUERY_STRING'];
	if(!empty($_GET['pg'])) {
		$arr = explode('&pg=', $attr);
		$attr = $arr[0];
	}
	else if(!empty($_GET['search'])) {
		$arr = explode('&search=', $attr);
		$attr = $arr[0];
	}
	echo "<script>location='index.php?".$attr."&search=".$_POST['txtsearch']."';</script>";
}
?>

<?php
if($_GET['pg'] == "") {
	$page = 0;
}
else {
	$page = ($_GET['pg']*10)-10;
}
$res = "SELECT * FROM tbluser WHERE user_level = '3'";
if(!empty($_GET['search'])) {
	$res .= " AND user_fullname LIKE '%".$_GET['search']."%'";
}
$res .= " ORDER BY user_register_date ASC";
$checkres = mysql_query($res, $dbLink);
$num = mysql_num_rows($checkres);
$max_page = ceil($num/10);
$res .= " LIMIT ".$page.",10";
$checkres = mysql_query($res, $dbLink);
?>

<div align="center">
<div class="products">
	<div class="container">
		<h1>List Emplyee</h1>
	</div>
</div>
<form action="" method="post" accept-charset="utf-8">
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
				<div class="pull-right"><input type="text" name="txtsearch" value="" placeholder="Employee Name"> <input type="submit" name="btnsearch" value="Search" class="btn btn-info btn-sm"></div> <br/>
		</div>
		<div class="col-md-2"></div>
	</div>
	<br/>
	<table class="table">
		<thead>
			<tr>
				<th>No.</th>
				<th>Date Joined</th>
				<th>Employee Name</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody id="table_content">
			<?php
				if(mysql_num_rows($checkres) > 0) {
					for($i=0; $i<mysql_num_rows($checkres); $i++) {
						$reg = mysql_fetch_array($checkres);
						$date_time = explode(' ', $reg['user_register_date']);
						$date = $date_time[0];
						$time = $date_time[1];
			?>
						<tr>
							<td><?php echo ($i+1)."."; ?></td>
							<td><span title="<?php echo $time; ?>"><?php echo $date; ?></span></td>
							<td><?php echo $reg['user_fullname']; ?></td>
							<td>
								<a class="btn btn-info btn-xs" href="index.php?id=emp_profile&mode=edit&uid=<?php echo $reg['user_id']; ?>" title="View Details"><i class="fa fa-list-alt"></i></a>
							</td>
						</tr>
			<?php 	}
				}
				else {
					echo "<tr><td></td><td></td><td>No Record(s) Found.</td><td></td></tr>";
				}
			?>
		</tbody>
	</table>
	</div> <div class='clearfix'> </div>
	<?php if($max_page > 1) {
		if(!empty($_GET['search'])) $search = "&search=".$_GET['search'];
	?>
	<div align="center">
		<ul class="pagination">
			<li><a href="index.php?id=list_emp&pg=1<?php echo $search ?>" <?php if($_GET['pg'] == 1 || empty($_GET['pg'])) echo "class='not-active'"; ?>><i class="fa fa-angle-double-left"></i></a></li>
			<li><a href="<?php if(empty($_GET['pg']) || $_GET['pg'] == 1) echo 'index.php?id=list_emp&pg=1'.$search; else echo 'index.php?id=list_emp&pg='.($_GET['pg']-1).$search; ?>" <?php if($_GET['pg'] == 1 || empty($_GET['pg'])) echo "class='not-active'"; ?>><i class="fa fa-angle-left"></i></a></li>
			<?php
				for($i=1; $i<=$max_page; $i++) {
					if(empty($_GET['pg']) && $i == 1) {
						echo "<li><a class='active' href='index.php?id=list_emp&pg=".$i.$search."'>".$i."</a></li>";
					}
					else if(!empty($_GET['pg']) && $_GET['pg'] == $i) {
						echo "<li><a class='active' href='index.php?id=list_emp&pg=".$i.$search."'>".$i."</a></li>";
					}
					else {
						echo "<li><a href='index.php?id=list_emp&pg=".$i.$search."'>".$i."</a></li>";
					}
				}
			?>
			<li><a href="<?php if(empty($_GET['pg']) || $_GET['pg'] == 1) echo 'index.php?id=list_emp&pg=2'.$search; else echo 'index.php?id=list_emp&pg='.($_GET['pg']+1).$search; ?>" <?php if($_GET['pg'] == $max_page) echo "class='not-active'"; ?>><i class="fa fa-angle-right"></i></a></li>
			<li><a href="index.php?id=list_emp&pg=<?php echo $max_page ?><?php echo $search ?>" <?php if($_GET['pg'] == $max_page) echo "class='not-active'"; ?>><i class="fa fa-angle-double-right"></i></a></li>
		</ul>
	</div>
	<?php } ?>
</form>
</div>

<?php
}
else {
	echo "<script>alert('Access Denied'); location='index.php';</script>";
}
?>