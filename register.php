<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Sign Up :: onCart</title>
</head>

<script src="http://www.google.com/jsapi"></script>
<script>
	google.load("jquery", "1.4.0");
</script>

<script type="text/javascript"> //used to change 'Full Name' or 'Company Name'
	$(function() {
		$('.acc_type').click(function () {
			var var_name = $("input[name='acc_type']:checked").val();
			$('#text_get').html(var_name);
			if ($("#rd2").is(":checked")) {
				$("#dvcom").show();
				$("#dvgst").show();
			}
			else {
				$("#dvcom").hide();
				$("#dvgst").hide();
			}
		});
	});
</script>

<?php
if(isset($_POST['btnreg'])) {
	if(trim($_POST['txtpassword']) == trim($_POST['txtconpassword'])) {
		$checkEmail = "SELECT * FROM tbluser WHERE user_email = '".trim($_POST['txtemail'])."'";
		$checkEmailResult = mysql_query($checkEmail, $dbLink);
		if(mysql_num_rows($checkEmailResult) > 0) {
			echo "<script>alert('E-mail Address Already Exists!'); window.history.back();</script>";
		}
		else {
			$user_level = ($_POST['acc_type'] == "Full Name" ? '0' : '2');
			$user_status = ($_POST['acc_type'] == "Full Name" ? '2' : '0');
			$sqlUser = "INSERT INTO tbluser(user_fullname, user_email, user_pass, user_gst_code, user_ic, user_level, user_status, user_register_date) VALUES('".ucwords(strtolower(trim($_POST['txtname'])))."', '".trim($_POST['txtemail'])."', '".md5(trim($_POST['txtpassword']))."', '".trim($_POST['txtgst'])."', '".trim($_POST['txtbsnum'])."', '".$user_level."', '".$user_status."', '".date("Y-m-d H:i:s")."')";
			$sqlUserResult = mysql_query($sqlUser, $dbLink);
			if($sqlUserResult) {
				if($_POST['acc_type'] == "Full Name") {
					$_SESSION['email'] = $_POST['txtemail'];
					echo "<script>alert('Sign up successfull!'); location='index.php';</script>";
				}
				else
					echo "<script>alert('Sign up successfull! Please wait 1 to 2 working days for Administrator or Employee to verify.');</script>";
			}
			else {
				echo "<script>alert('Sign up failed!'); location='index.php?id=login';</script>";
			}
		}
	}
	else {
		echo "<script>alert('Password does not matched!'); window.history.back();</script>";
	}
}
?>

<div class="container">
	<div class="register">
		<h1>Register</h1>
			<form id="form_signup" name="form_signup" method="post" action="">
			<div class="col-md-6  register-top-grid">
				<div class="mation" id="TextBoxContainer">
					<span id="text_get">Full Name</span>
					<input type="text" name="txtname" maxlength="50" required="required"/>
				 
					<span>Email Address</span>
					<input type="email" name="txtemail" maxlength="50" required="required"/>

					<div id="dvcom" style="display: none">
						<span>Business License (leave blank if does not have)</span>
						<input type="text" name="txtbsnum" id="txtbsnum" maxlength="16" />
					</div>
						 
					<span>Account Type</span>
					<div class="col-md-4">
						<input type="radio" name="acc_type" id="rd1" class="acc_type" value="Full Name" checked /><label for="rd1" class="radio">Customer</label>
					</div>
					<div class="col-md-4">
						<input type="radio" name="acc_type" id="rd2" class="acc_type" value="Company or Seller Name" /><label for="rd2" class="radio">Dealer</label>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class=" col-md-6 register-bottom-grid">
				<div class="mation" id="TextBoxContainer">
					<span>Password</span>
					<input type="password" name="txtpassword" maxlength="32" required="required"/>

					<span>Confirm Password</span>
					<input type="password" name="txtconpassword" maxlength="32" required="required"/>

					<div id="dvgst" style="display: none">
						<span>GST Number (leave blank if does not have)</span>
						<input type="text" name="txtgst" id="txtgst" maxlength="10" />
					</div>
				</div>
			</div>
			<div class="clearfix"> </div>
				
			<div class="register-but">
				<input type="submit" value="submit" name="btnreg">
				<div class="clearfix"> </div>
			</div>
		</form>
	</div>
</div>