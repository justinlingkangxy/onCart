<?php session_start(); ?>
<?php if(!empty($_SESSION['email']) && ($_SESSION['level'] == "admin" || $_SESSION['level'] == "seller")) { ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Monthly Sales Report Details :: onCart</title>
</head>

<?php
if($_GET['pg'] == "") {
	$page = 0;
}
else {
	$page = ($_GET['pg']*10)-10;
}
$res = "SELECT tblorder.*, tblproduct.prod_name FROM tblorder, tblproduct WHERE tblorder.order_status > 1 AND tblorder.order_num = '".$_GET['oid']."' AND tblproduct.user_email = '".$_SESSION['email']."' AND tblorder.prod_code = tblproduct.prod_code ORDER BY order_date_add DESC";
$checkres = mysql_query($res, $dbLink);
$num = mysql_num_rows($checkres);
$max_page = ceil($num/10);
$res = " LIMIT ".$page.",10";
$checkres = mysql_query($res, $dbLink);
?>

<div align="center">
<div class="products">
	<div class="container">
		<h1>Sales Report Details</h1>
	</div>
</div>
<form action="" method="post" accept-charset="utf-8">
	<table class="table">
		<thead>
			<tr>
				<th>Date Added</th>
				<th>Product Name</th>
				<th>Quantities</th>
				<th>Sales</th>
			</tr>
		</thead>
		<tbody id="table_content">
			<?php
				if(mysql_num_rows($checkres) > 0) {
					for($i=0; $i<mysql_num_rows($checkres); $i++) {
						$reg = mysql_fetch_array($checkres);
						$date_time = explode(' ', $reg['order_date_add']);
						$date = $date_time[0];
						$time = $date_time[1];
			?>
						<tr>
							<td><span title="<?php echo $time; ?>"><?php echo $date; ?></span></td>
							<td><?php echo $reg['prod_name']; ?></td>
							<td><?php echo $reg['order_prod_qty']; ?></td>
							<td>RM <?php echo $reg['order_sub_total']; ?></td>
						</tr>
			<?php 	}
				}
				else {
					echo "<tr><td></td><td>No Record(s) Found.</td><td></td><td></td></tr>";
				}
			?>
		</tbody>
	</table>
	</div> <div class='clearfix'> </div>
	<?php if($max_page > 1) { ?>
	<div align="center">
		<ul class="pagination">
			<li><a href="index.php?id=sales_report_details&pg=1" <?php if($_GET['pg'] == 1 || empty($_GET['pg'])) echo "class='not-active'"; ?>><i class="fa fa-angle-double-left"></i></a></li>
			<li><a href="<?php if(empty($_GET['pg']) || $_GET['pg'] == 1) echo 'index.php?id=sales_report_details&pg=1'; else echo 'index.php?id=sales_report_details&pg='.($_GET['pg']-1); ?>" <?php if($_GET['pg'] == 1 || empty($_GET['pg'])) echo "class='not-active'"; ?>><i class="fa fa-angle-left"></i></a></li>
			<?php
				for($i=1; $i<=$max_page; $i++) {
					if(empty($_GET['pg']) && $i == 1) {
						echo "<li><a class='active' href='index.php?id=sales_report_details&pg=".$i."'>".$i."</a></li>";
					}
					else if(!empty($_GET['pg']) && $_GET['pg'] == $i) {
						echo "<li><a class='active' href='index.php?id=sales_report_details&pg=".$i."'>".$i."</a></li>";
					}
					else {
						echo "<li><a href='index.php?id=sales_report_details&pg=".$i."'>".$i."</a></li>";
					}
				}
				
			<li><a href="<?php if(empty($_GET['pg']) || $_GET['pg'] == 1) echo 'index.php?id=sales_report_details&pg=2'; else echo 'index.php?id=sales_report_details&pg='.($_GET['pg']+1); ?>" <?php if($_GET['pg'] == $max_page) echo "class='not-active'"; ?>><i class="fa fa-angle-right"></i></a></li>
			<li><a href="index.php?id=sales_report_details&pg=<?php echo $max_page ?>" <?php if($_GET['pg'] == $max_page) echo "class='not-active'"; ?>><i class="fa fa-angle-double-right"></i></a></li>
		</ul>
	</div>
	<?php } ?>
</form>
</div>

<?php
}
else {
	echo "<script>alert('Access Denied'); location='index.php';</script>";
}
?>