<?php session_start(); ?>
<?php if(!empty($_SESSION['email']) && ($_SESSION['level'] == "admin" || $_SESSION['level'] == "seller")) { ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title><?php if(!empty($_GET['mode']) && $_GET['mode'] == "edit") echo "Edit"; else echo "Register"; ?> Product :: onCart</title>
</head>

<?php
$res = "SELECT user_gst_code FROM tbluser WHERE user_email = '".$_SESSION['email']."'";
$checkRes = mysql_query($res, $dbLink);
$user = mysql_fetch_array($checkRes);
$user_gst_code = $user['user_gst_code'];

if($_GET['mode'] == 'edit') {
	$res = "SELECT * FROM tblproduct WHERE user_email = '".$_SESSION['email']."' AND prod_code = '".$_GET['pcode']."'";
	$checkres = mysql_query($res, $dbLink);
	if ($checkres) {
		$reg = mysql_fetch_array($checkres);
	}
}
?>

<?php
if(isset($_POST['btnupdate'])) {
	if($_POST['sel_cat'] != "") {
		$prod_cond = ($_POST['prod_cond'] == "New" || empty($_POST['prod_cond']) ? 0 : 1);
		$prod_gst = (($_POST['chkgst'] == "1") ? 1.06 : 1.00);
		$prod_gst_code = ($_POST['chkgst'] == "1" ? $user_gst_code : "");
		$prod_fprice = $_POST['txtfprice'];
		$prod_profit = $_POST['txtprofit'];
		if(empty($_POST['txtqty']))
			$prod_quantity = -1;
		else
			$prod_quantity = $_POST['txtqty'];
		$sqlProd = "UPDATE tblproduct SET prod_name = '".ucwords(strtolower($_POST['txtname']))."', prod_brands = '".ucfirst(strtolower($_POST['txtbrand']))."', prod_category = '".$_POST['sel_cat']."', prod_qty = '".$prod_quantity."', prod_condition = '".$prod_cond."', prod_sell_price = '".trim($_POST['txtsprice'])."', prod_buy_price = '".trim($_POST['txtbprice'])."', prod_profit = '".$prod_profit."', prod_gst = '".$prod_gst."', prod_gst_code = '".$prod_gst_code."', prod_final_sell_price = '".$prod_fprice."', prod_width = '".trim($_POST['txtwidth'])."', prod_length = '".trim($_POST['txtlength'])."', prod_height = '".trim($_POST['txtheight'])."', prod_weight = '".trim($_POST['txtweight'])."', prod_desc = '".trim($_POST['txadesc'])."', prod_date_upd = '".date("Y-m-d H:i:s")."' WHERE prod_code = '".$_GET['pcode']."'";
		$sqlProdResult = mysql_query($sqlProd, $dbLink);
		if($sqlProdResult) {
				echo "<script>alert('Product update successfull.'); window.history.back();</script>";
		}
		else {
			echo "<script>alert('Product update failed!'); window.history.back();</script>";
		}
	}
	else {
		echo "<script>alert('Please select product category!'); window.history.back();</script>";
	}
}
?>

<?php
if(isset($_POST['btnadd'])) {
	if($_POST['sel_cat'] != "") {
		$prod_cond = ($_POST['prod_cond'] == "New" || empty($_POST['prod_cond']) ? 0 : 1);
		$prod_gst = ($_POST['chkgst'] == "1" ? 1.06 : 1.00);
		$prod_gst_code = ($_POST['chkgst'] == "1" ? $user_gst_code : "");
		$prod_fprice = $_POST['txtfprice'];
		$prod_profit = $_POST['txtprofit'];
		if(empty($_POST['txtqty']))
			$prod_quantity = -1;
		else
			$prod_quantity = $_POST['txtqty'];

		$t = microtime(true);
		$micro = sprintf("%06d",($t - floor($t)) * 1000000);
		$d = new DateTime( date('y-m-d H:i:s.'.$micro, $t) );
		$prod_code = $d->format("ymdHisu");
		$sqlProd = "INSERT INTO tblproduct(user_email, prod_name, prod_code, prod_brands, prod_category, prod_qty, prod_condition, prod_sell_price, prod_buy_price, prod_profit, prod_gst, prod_gst_code, prod_final_sell_price, prod_width, prod_length, prod_height, prod_weight, prod_desc, prod_date_add) VALUES('".$_SESSION['email']."', '".ucwords(strtolower($_POST['txtname']))."', '".$prod_code."', '".ucfirst(strtolower($_POST['txtbrand']))."', '".$_POST['sel_cat']."', '".$prod_quantity."', '".$prod_cond."', '".trim($_POST['txtsprice'])."', '".trim($_POST['txtbprice'])."', '".$prod_profit."', '".$prod_gst."', '".$prod_gst_code."', '".$prod_fprice."', '".trim($_POST['txtwidth'])."', '".trim($_POST['txtlength'])."', '".trim($_POST['txtheight'])."', '".trim($_POST['txtweight'])."', '".trim($_POST['txadesc'])."', '".date("Y-m-d H:i:s")."')";
		$sqlProdResult = mysql_query($sqlProd, $dbLink);
		if($sqlProdResult) {
				echo "<script>alert('Product added successfully.'); location='index.php?id=reg_prod';</script>";
		}
		else {
			echo "<script>alert('Product added failed!'); window.history.back();</script>";
		}
	}
	else {
		echo "<script>alert('Please select product category!'); window.history.back();</script>";
	}
}
?>

<div class="container">
	<div class="register">
		<h1><?php if(!empty($_GET['mode']) && $_GET['mode'] == "edit") echo "Edit"; else echo "Register"; ?> Product</h1>
		<form id="form_register_prod" name="form_register_prod" method="post" action="" enctype="multipart/form-data">
			<div class="col-md-6  register-top-grid">
				<div class="mation">
					<fieldset><legend>General Info</legend>
						<span id="text_get">Product Name</span>
						<input type="text" name="txtname" required="required" maxlength="50" value="<?php echo $reg['prod_name']; ?>" />

						<span>Brand</span>
						<input type="text" name="txtbrand" maxlength="50" value="<?php echo $reg['prod_brands']; ?>" />
	
						<div class="col-md-6">
							<span>Category</span>
							<select class="form-group-lg form-control" style="margin-top: 12px" name="sel_cat" id="sel_cat">
								<option value="">Please select...</option>
								<?php
									$catres = "SELECT cat_name FROM tblcategory GROUP BY cat_name";
									$checkcatres = mysql_query($catres, $dbLink);
									while($row = mysql_fetch_array($checkcatres))
									{
										echo "<optgroup label='".$row['cat_name']."'>";
										$subcatres = "SELECT cat_id, cat_subname FROM tblcategory WHERE cat_name = '".$row['cat_name']."'";
										$checksubcatres = mysql_query($subcatres, $dbLink);
										while($subrow = mysql_fetch_array($checksubcatres))
										{
											if($subrow['cat_id'] == $reg['prod_category'])
												echo "<option selected='selected' value='".$subrow['cat_id']."'>".$subrow['cat_subname']."</option>";
											else
												echo "<option value='".$subrow['cat_id']."'>".$subrow['cat_subname']."</option>";
										}
										echo "</optgroup>";
									}
								?>
							</select>
						</div>

						<div class="col-md-6">
							<span>Quantities (leave blank if unlimited)</span>
							<input type="number" min="-1" name="txtqty" id="txtqty" maxlength="11" value="<?php if($reg['prod_qty'] == "-1") echo ""; else echo $reg['prod_qty']; ?>" />
						</div>
							 
						<span>Condition</span>
						<div class="col-md-4">
							<input type="radio" name="prod_cond" id="rd1" value="New" <?php if($reg['prod_condition'] == "0") echo "checked='checked'"; ?> /><label for="rd1" class="radio">New</label>
						</div>
						<div class="col-md-4">
							<input type="radio" name="prod_cond" id="rd2" value="Used" <?php if($reg['prod_condition'] == "1") echo "checked='checked'"; ?> /><label for="rd2" class="radio">Used</label>
						</div>
					</fieldset>
				</div>
				<div class="clearfix"> </div>
			</div>
			<div class="col-md-6 register-bottom-grid">
				<div class="mation">
					<fieldset><legend>Price</legend>
						<div class="col-md-6">
							<span>Selling Price (RM)</span>
							<input type="number" min="0" step="any" name="txtsprice" id="txtsprice" required="required" value="<?php echo $reg['prod_sell_price']; ?>" />
						</div>
	
						<div class="col-md-6">
							<span>Costs (RM)</span>
							<input type="number" min="0" step="any" name="txtbprice" id="txtbprice" value="<?php echo $reg['prod_buy_price']; ?>" />
						</div>

						<?php if(!empty($user_gst_code)) { ?>
						<div class="col-md-6">
							<span>Goods and Service Tax (GST)</span>
							<label class="checkbox"><input type="checkbox" name="chkgst" id="chkgst" value="1" <?php if($reg['prod_gst'] == "1.06") echo "checked='checked'"; ?> /><i> </i>Enable GST</label>
						</div>
						<?php } ?>

						<div class="col-md-6">
							<span>Profit (RM)</span>
							<input type="text" name="txtprofit" id="txtprofit" readonly="readonly" value="<?php echo $reg['prod_profit']; ?>" />
						</div>

						<div class="col-md-6">
							<span>Final Selling Price (RM)</span>
							<input type="text" name="txtfprice" id="txtfprice" readonly="readonly" value="<?php echo $reg['prod_final_sell_price']; ?>" />
						</div>
					</fieldset>

					<br/>

					<fieldset><legend>Dimension</legend>
						<div class="col-md-6">
							<span>Width (cm)</span>
							<input type="number" min="0" step="any" name="txtwidth" id="txtwidth" value="<?php echo $reg['prod_width']; ?>" />
						</div>
	
						<div class="col-md-6">
							<span>Length (cm)</span>
							<input type="number" min="0" step="any" name="txtlength" id="txtlength" value="<?php echo $reg['prod_length']; ?>" />
						</div>

						<div class="col-md-6">
							<span>Height (cm)</span>
							<input type="number" min="0" step="any" name="txtheight" id="txtheight" value="<?php echo $reg['prod_height']; ?>" />
						</div>

						<div class="col-md-6">
							<span>Weight (kg)</span>
							<input type="number" min="0" step="any" name="txtweight" id="txtweight" required="required" value="<?php echo $reg['prod_weight']; ?>" />
						</div>
					</fieldset>
				</div>
			</div>

			<div class="col-md-12">
				<div class="mation">
					<fieldset><legend>Product Description</legend>
						<textarea name="txadesc" style="width:100%; height:100px"><?php echo $reg['prod_desc']; ?></textarea>
					</fieldset>
				</div>
			</div>
			<div class="clearfix"> </div>

			<?php if($_GET['mode'] == 'edit') { ?>
				<div class="col-md-12">
					<div class="mation">
						<fieldset><legend>Upload Product Image</legend>
							<input type="file" name="img" value="" placeholder="choose your product image">
							<div class="register-but">
								<input type="submit" name="btnupload" value="Upload">
							</div>
   						</fieldset>
					</div>
				</div>
				<div class="clearfix"> </div>
			<?php
				if(isset($_POST['btnupload'])) {
					$getimg = "SELECT * FROM tblimage WHERE img_code = '".$_GET['pcode']."'";
					$getimgResult = mysql_query($getimg, $dbLink);
					$img_name = mysql_fetch_array($getimgResult);
					$file_name = $_FILES['img']['name'];

					if($file_name != "") {
						$file_type = $_FILES['img']['type'];
						$allow_ext = array("jpg", "jpeg", "png", "gif");
						$ext = end(explode(".", $file_name));

						if(in_array(strtolower($ext), $allow_ext)) { //check file is invalid type*/
							$file_size = $_FILES['img']['size'];

							if($file_size < 10000000) { //check file is less than 10MB
								if(!empty($img_name['img_name'])) {
									$imgnum = mysql_num_rows($getimgResult)+1;
									$file_new_name = $_GET['pcode'].$imgnum.".".$ext; //rename file
								}
								else {
									$file_new_name = $_GET['pcode']."1.".$ext; //rename file
								}
								$file_tmp_name = $_FILES['img']['tmp_name'];
								$path = "prod_images/".$file_new_name;

								if(move_uploaded_file($file_tmp_name, $path)) {
									$upprod = "UPDATE tblproduct SET prod_date_upd = '".date("Y-m-d H:i:s")."' WHERE prod_code = '".$_GET['pcode']."'";
									$upprodResult = mysql_query($upprod, $dbLink);
									$upimg = "INSERT INTO tblimage(img_name, img_code) VALUES('".$file_new_name."', '".$_GET['pcode']."')";
									$upimgResult = mysql_query($upimg, $dbLink);
									echo "<script>location='index.php?".$_SERVER['QUERY_STRING']."';</script>";
								}
								else {
									echo "<script>alert('Image upload failed!');</script>";
								}
							}
							else {
								echo "<script>alert('Image should be less than 10MB!');</script>";
							}
						}
						else {
							echo "<script>alert('Invalid image type!');</script>";
						}
					}
					else {
						echo "<script>alert('Please select an image!');</script>";
					}
				}

				$getimg = "SELECT img_name FROM tblimage WHERE img_code = '".$_GET['pcode']."'";
				$getimgResult = mysql_query($getimg, $dbLink);
				if(mysql_num_rows($getimgResult) > 0) {
					for($i=0; $i<mysql_num_rows($getimgResult); $i++) {
						$img_name = mysql_fetch_array($getimgResult);
						echo "<img src='prod_images/".$img_name['img_name']."' width='auto' height='100px' alt=''>";
					}
				}
				else {
					echo "<img src='images/no_image.jpg' width='auto' height='100px' alt=''>";
				}
			}
			?>
				
			<div class="register-but">
				<input type="submit" <?php if($_GET['mode'] == 'edit') {echo "value='Update' name='btnupdate'";} else {echo "value='Add' name='btnadd'";} ?> >
				<div class="clearfix"> </div>
			</div>
		</form>
	</div>
</div>

<script>
$(document).ready(function()
{
	//function to calculate profit
	var calculateProfit = function(){
		var profit = $('#txtsprice').val() - $('#txtbprice').val();
		$('#txtprofit').val(profit.toFixed(2));
	};

	//funtion to calculate gst
	var calculateGST = function(){
		var gst_price = $('#txtsprice').val() * 1.06;
		$('#txtfprice').val(gst_price.toFixed(2));
	};

	$('#txtbprice').keyup(function(){
	    calculateProfit();
	});

	$('#txtsprice').keyup(function(){
		if($('#chkgst').is(':checked')) {
			calculateGST();
		}
		else {
			$('#txtfprice').val($('#txtsprice').val());
		}
	    calculateProfit();
	});

	$('#chkgst').on('click', function(e){
		if($(this).is(':checked'))
		{			
			var cost = $('#txtbprice').val();
			var sell = $('#txtsprice').val();
			var price = $('#txtfprice').val();
			var gst_price = sell * 1.06;
			if(sell != '')
			{
				$('#txtfprice').val(gst_price.toFixed(2));
			}
			if(cost != '')
			{
				var profit = sell - cost;
				$('#txtprofit').val(profit.toFixed(2));
			}
			else if(cost == '')
			{
				var profit = sell;
				$('#txtprofit').val(profit.toFixed(2));
			}
		}
		else
		{
			$('#txtfprice').val($('#txtsprice').val());
		}
	});

	$('#chkseasondis').on('click', function(e){
		if($(this).is(':checked'))
		{			
			$("#dvseasondis").show();
		}
		else
		{
			$("#dvseasondis").hide();
		}
	});

	$('input[name=txtsprice], input[name=chkgst]').on('keyup blur changed ifChecked ifUnchecked', function(){
		var value = parseFloat($('input[name=txtsprice]').val());
		if($('input[name=chkgst]').is(':checked'))
			value = value * 1.06;
		if(! isNaN(value))
			$('#txtfprice').val(value.toFixed(2));
		else
			$('#txtfprice').val("0.00");
	});

	$('input[name=txtsprice], input[name=txtbprice]').on('keyup blur changed', function(){
		var cost = $('#txtbprice').val();
		var value;
		if(cost != '')
			value = parseFloat($('input[name=txtsprice]').val()) - parseFloat($('input[name=txtbprice]').val());
		else if(cost == '')
			value = parseFloat($('input[name=txtsprice]').val());
		if(! isNaN(value))
			$('#txtprofit').val(value.toFixed(2));
		else
			$('#txtprofit').val("0.00");
	});
});
</script>
<?php
}
else {
	echo "<script>alert('Access Denied'); location='index.php';</script>";
}
?>