<?php session_start(); ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Product Details :: onCart</title>
</head>

<script src="js/imagezoom.js"></script>
<script type="text/javascript">
	$(function() {
	    var menu_ul = $('.menu-drop > li > ul'), menu_a  = $('.menu-drop > li > a');
	    menu_ul.hide();
	    menu_a.click(function(e) {
	        e.preventDefault();
	        if(!$(this).hasClass('active')) {
	            menu_a.removeClass('active');
	            menu_ul.filter(':visible').slideUp('normal');
	            $(this).addClass('active').next().stop(true,true).slideDown('normal');
	        } else {
	            $(this).removeClass('active');
	            $(this).next().stop(true,true).slideUp('normal');
	        }
	    });
	});
</script>
<!-- FlexSlider -->
<script defer src="js/jquery.flexslider.js"></script>
<link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen" />

<script>
$(window).load(function() {
	$('.flexslider').flexslider({
		animation: "slide",
		controlNav: "thumbnails"
	});
});
</script>
	<link href="css/popuo-box.css" rel="stylesheet" type="text/css" media="all"/>
	<script src="js/jquery.magnific-popup.js" type="text/javascript"></script>
<script>
	$(document).ready(function() {
		$('.popup-with-zoom-anim').magnificPopup({
			type: 'inline',
			fixedContentPos: false,
			fixedBgPos: true,
			overflowY: 'auto',
			closeBtnInside: true,
			preloader: false,
			midClick: true,
			removalDelay: 300,
			mainClass: 'my-mfp-zoom-in'
		});															
	});
</script>

<?php
$res = "SELECT * FROM tblproduct WHERE prod_code = '".$_GET['pcode']."'";
$checkres = mysql_query($res, $dbLink);
if ($checkres) {
	$reg = mysql_fetch_array($checkres);
}
?>

<?php
if(isset($_POST['btnwish'])) {
	if(empty($_SESSION['email'])) {
		echo "<script>alert('Please login.'); location='index.php?id=login';</script>";
	}
	else {
		$inwishres = "INSERT INTO tblwish(prod_code, user_email, wish_date_add) VALUES('".$_POST['btnwish']."', '".$_SESSION['email']."', '".date("Y-m-d H:i:s")."')";
		$checkinwishres = mysql_query($inwishres, $dbLink);
		echo "<script>alert('Item added into wish list.'); location='index.php?".$_SERVER['QUERY_STRING']."';</script>";
	}
}

if(isset($_POST['btnaddcart'])) {
	if(empty($_SESSION['email'])) {
		echo "<script>alert('Please login.'); location='index.php?id=login';</script>";
	}
	else {
		$countcart = "SELECT COUNT(cart_id) AS RecordNum FROM tblcart WHERE prod_code = '".$_POST['btnaddcart']."' AND user_email = '".$_SESSION['email']."'";
		$countcartres = mysql_query($countcart, $dbLink);
		$Row = mysql_fetch_array($countcartres);
		if($Row['RecordNum'] > 0) {
			$checkQuery = "SELECT * FROM tblcart, tblproduct WHERE tblcart.prod_code = '".$_POST['btnaddcart']."' AND tblcart.prod_code = tblproduct.prod_code AND tblcart.user_email = '".$_SESSION['email']."'";
			$checkQueryRes = mysql_query($checkQuery, $dbLink);
			if(mysql_num_rows($checkQueryRes) > 0) {
				$reg = mysql_fetch_array($checkQueryRes);
				$new_qty = ($reg['cart_prod_qty']+$_POST['txtnum']);
				$sub_total = $reg['prod_final_sell_price']*$new_qty;
				$updcart = "UPDATE tblcart SET cart_prod_qty = '".$new_qty."', cart_sub_total = '".$sub_total."', cart_date_upd = '".date("Y-m-d H:i:s")."' WHERE prod_code = '".$reg['prod_code']."'";
				$updcartresult = mysql_query($updcart, $dbLink);
				echo "<script>location='index.php?".$_SERVER['QUERY_STRING']."';</script>";
			}
		}
		else {
			$checkQuery = "SELECT prod_final_sell_price, prod_weight FROM tblproduct WHERE prod_code = '".$_POST['btnaddcart']."'";
			$checkQueryRes = mysql_query($checkQuery, $dbLink);
			$reg = mysql_fetch_array($checkQueryRes);
			$sqlCart = "INSERT INTO tblcart(prod_code, user_email, cart_prod_qty, cart_prod_weight, cart_sub_total, cart_date_add) VALUES('".$_POST['btnaddcart']."', '".$_SESSION['email']."', '".$_POST['txtnum']."', '".$reg['prod_weight']."', '".$reg['prod_final_sell_price']."', '".date("Y-m-d H:i:s")."')";
			$sqlCartResult = mysql_query($sqlCart, $dbLink);
			echo "<script>location='index.php?".$_SERVER['QUERY_STRING']."';</script>";
		}
	}
}
?>

<form action="" method="post" name="form_prod_detail" id="form_prod_detail">
<div class="single">
	<div class="container">
		<div class="col-md-9">
			<div class="col-md-5 grid">		
				<div class="flexslider">
					<ul class="slides">
						<?php
						$getimg = "SELECT img_name FROM tblimage WHERE img_code = '".$_GET['pcode']."'";
						$getimgResult = mysql_query($getimg, $dbLink);
						if(mysql_num_rows($getimgResult) > 0) {
							for($i=0; $i<mysql_num_rows($getimgResult); $i++) {
								$img_name = mysql_fetch_array($getimgResult);
						?>
								<li data-thumb="prod_images/<?php echo $img_name['img_name'] ?>">
									<div class="thumb-image"> <img src="prod_images/<?php echo $img_name['img_name'] ?>" data-imagezoom="true" class="img-product"> </div>
								</li>
						<?php
							}
						}
						else {
							echo "<li data-thumb='images/no_image.jpg'><img class='img-responsive' src='images/no_image.jpg' alt=''></li>";
						}
						?>
					</ul>
				</div>
			</div>

			<div class="col-md-7 single-top-in">
				<div class="single-para simpleCart_shelfItem">
					<h1>
						<?php echo $reg['prod_name'] ?>&nbsp;
						<?php
							$wishres = "SELECT prod_code FROM tblwish WHERE prod_code = '".$reg['prod_code']."' AND user_email = '".$_SESSION['email']."'";
							$checkwishres = mysql_query($wishres, $dbLink);
							$wishreg = mysql_fetch_array($checkwishres);
							if(mysql_num_rows($checkwishres) == 0) {
						?>
							<button type="submit" name="btnwish" title="Add to Favourite" class="btn btn-xs btn-danger" value="<?php echo $reg['prod_code']; ?>"><i class="fa fa-heart"></i></button>
						<?php } ?>
					</h1>
					<p><?php echo $reg['prod_desc'] ?></p>
					<label class="add-to item_price">RM <?php echo $reg['prod_final_sell_price']; ?><?php if($reg['prod_gst'] == '1.06') echo " <span style='font-size:14px'>(inclusive GST)</span>"; ?></label>
					<label>Stock: <?php if($reg['prod_qty'] != '-1') echo $reg['prod_qty']; else echo "Unlimited"; ?></label><br/><br/>
					<div class="row">
						<?php if($reg['prod_width'] != 0) { ?>
						<div class="col-md-6">
							<label>Width: <?php echo $reg['prod_width']; ?> cm</label>
						</div>
						<?php } ?>
						<?php if($reg['prod_length'] != 0) { ?>
						<div class="col-md-6">
							<label>Length: <?php echo $reg['prod_length']; ?> cm</label>
						</div>
						<?php } ?>
						<?php if($reg['prod_height'] != 0) { ?>
						<div class="col-md-6">
							<label>Height: <?php echo $reg['prod_height']; ?> cm</label>
						</div>
						<?php } ?>
						<?php if($reg['prod_weight'] != 0) { ?>
						<div class="col-md-6">
							<label>Weight: <?php echo $reg['prod_weight']; ?> kg</label>
						</div>
						<?php } ?>
					</div>
					<div class="clearfix"> </div>
					<input type="number" name="txtnum" min="1" <?php if($reg['prod_qty'] != '-1') echo "max='".$reg['prod_qty']."'"; ?> value="1" style="width:80px">
					<button type="submit" name="btnaddcart" class="btn btn-1 btn-info" value="<?php echo $reg['prod_code']; ?>">Add To Cart</button>
				</div>
			</div>
		</div>
		<div class="col-md-3 product-bottom">
			<div class="product-bottom">
				<h3 class="cate">Related Product</h3>
				<?php
					$sellerres = "SELECT prod_category FROM tblproduct WHERE prod_code = '".$_GET['pcode']."'";
					$checksellerres = mysql_query($sellerres, $dbLink);
					$sellerreg = mysql_fetch_array($checksellerres);
					$relares = "SELECT prod_code, prod_name, prod_final_sell_price FROM tblproduct WHERE prod_code != '".$_GET['pcode']."' AND prod_category = '".$sellerreg['prod_category']."' ORDER BY RAND() LIMIT 0, 4";
					$checkrelares = mysql_query($relares, $dbLink);
					if(mysql_num_rows($checkrelares) > 0) {
						for($i=0; $i<4; $i++) {
						$relareg = mysql_fetch_array($checkrelares);
				?>
						<div class="product-go">
							<div class="fashion-grid">
								<a href="index.php?id=single&pcode=<?php echo $relareg['prod_code'] ?>">
									<?php
									$getimg = "SELECT img_name FROM tblimage WHERE img_code = '".$relareg['prod_code']."' AND img_name LIKE '".$relareg['prod_code']."1%'		";
									$getimgResult = mysql_query($getimg, $dbLink);
									$img_name = mysql_fetch_array($getimgResult);
									if(!empty($img_name['img_name'])) {
										echo "<img class='img-responsive' src='prod_images/".$img_name['img_name']."' alt=''>";
									}
									else {
										echo "<img class='img-responsive' src='images/no_image.jpg' alt=''>";
									}
									?>
								</a>
							</div>
							<div class="fashion-grid1">
								<h6 class="best2"><a href="index.php?id=single&pcode=<?php echo $relareg['prod_code'] ?>"><?php echo $relareg['prod_name']; ?></a></h6>
								<span class="price-in1">RM <?php echo $relareg['prod_final_sell_price']; ?></span>
							</div>
							<div class="clearfix"> </div>
						</div>
				<?php
						}
					}
				?>
			</div>
		</div>
		<div class="clearfix"> </div>
	</div>
</div>
</form>

<?php
$userres = "SELECT user_fullname, user_email FROM tbluser WHERE user_email = '".$_SESSION['email']."'";
$checkuserres = mysql_query($userres, $dbLink);
if ($checkuserres) {
	$userreg = mysql_fetch_array($checkuserres);
}
?>
<div class="footer">
<div class="container">
	<div class="register">
		<form id="form_comment" name="form_comment" method="post" action="">
			<div class="col-md-12  register-top-grid">
				<div class="mation">
					<div class="col-md-6">
					<fieldset><legend>Write Review</legend>
						<div class="col-md-6">
							<span>Name</span>
							<input type="text" name="txtcommname" id="txtcommname" <?php if(!empty($_SESSION['email'])) echo "readonly='readonly'"; ?> value="<?php echo $userreg['user_fullname']; ?>" />
						</div>
						<div class="col-md-6">
							<span>Email</span>
							<input type="email" name="txtcommemail" id="txtcommemail" <?php if(!empty($_SESSION['email'])) echo "readonly='readonly'"; ?> value="<?php echo $userreg['user_email']; ?>" />
						</div>
						<span>Comment</span>
						<textarea name="txacomment" id="txacomment" style="width:100%; height:100px" maxlength="80"></textarea>
						<div class="clearfix"> </div>
						<div class="register-but">
							<input type="submit" name="btnpost" value="Post">
							<div class="clearfix"> </div>
						</div>
   					</fieldset>
   					</div>
   					<div class="col-md-6">
					<fieldset><legend>Review</legend>
						<?php
						$commres = "SELECT comment_name, comment_desc, comment_date_add FROM tblcomment WHERE prod_code = '".$_GET['pcode']."' ORDER BY comment_date_add DESC";
						$checkcommres = mysql_query($commres, $dbLink);
						$i = 1;
						while($row = mysql_fetch_array($checkcommres))
						{
							$name = $row['comment_name'];
							$comment = $row['comment_desc'];
							$time = $row['comment_date_add'];
						?>
							<div class="comment">
								<div class="name">Posted By: <?php echo $name;?></div>
								<div class="date"><?php echo $time;?></div>
								<p><?php echo $comment;?></p>
							</div>
						<?php
							$i++;
						}
						?>
   					</fieldset>
   					</div>
   					<div class="clearfix"> </div>
				</div>
			</div>
		</form>
	</div>
</div>
</div>

<?php
	if(isset($_POST['btnpost']))
	{
		$comment = $_POST['txacomment'];
		$name = $_POST['txtcommname'];
		$email = $_POST['txtcommemail'];

		$sqlComm = "INSERT INTO tblcomment(prod_code, comment_name, comment_email, comment_desc, comment_date_add) VALUES('".$_GET['pcode']."', '".$name."', '".$email."', '".$comment."', '".date("Y-m-d H:i:s")."')";
		$sqlCommResult = mysql_query($sqlComm, $dbLink);
		
		echo "<script>location='index.php?".$_SERVER['QUERY_STRING']."';</script>";
	}
?>