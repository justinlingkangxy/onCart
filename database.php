<?php
	$tableList = array(
		"CREATE TABLE tbluser (
			user_id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
			user_fullname varchar(50) NOT NULL DEFAULT '' COMMENT 'also used as company name',
			user_ic varchar(16) NOT NULL DEFAULT '' COMMENT 'also used as business license',
			user_email varchar(50) NOT NULL UNIQUE KEY,
			user_mphone varchar(20) NOT NULL,
			user_pass varchar(32) NOT NULL,
			user_address varchar(255) NOT NULL DEFAULT '',
			user_city varchar(20) NOT NULL DEFAULT '',
			user_postcode varchar(7) NOT NULL DEFAULT '00000',
			user_state varchar(30) NOT NULL DEFAULT '',
			user_country varchar(30) NOT NULL DEFAULT 'Malaysia',
			user_gender tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=Unknown, 1=Male, 2=Female',
			user_birthdate date NOT NULL DEFAULT '0000-00-00',
			user_gst_code varchar(10) DEFAULT '' COMMENT 'if empty the company will be no gst',
			user_contact_person varchar(50) DEFAULT '',
			user_contact_person_phone varchar(20) DEFAULT '',
			user_level tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=Buyer, 1=VIP Buyer, 2=Seller, 3=Employee, 4=Admin',
			user_status tinyint(1) NOT NULL DEFAULT '0' COMMENT '-1=left, 0=unverified, 1=rejected, 2=verified, 3=admin',
			user_register_date datetime NOT NULL DEFAULT '0000-00-00 00:00:00')",

		"CREATE TABLE tblproduct (
			prod_id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
			user_email varchar(50) NOT NULL COMMENT 'seller email',
			prod_name varchar(50) NOT NULL DEFAULT '',
			prod_code varchar(18) NOT NULL UNIQUE KEY DEFAULT '000000000000000000' COMMENT 'combined with year, month, day, hour, mins, sec and micro sec',
			prod_category int(11) NOT NULL DEFAULT '1',
			prod_brands varchar(50) NOT NULL DEFAULT '',
			prod_width decimal(20,2) NOT NULL DEFAULT '0.00',
			prod_length decimal(20,2) NOT NULL DEFAULT '0.00',
			prod_height decimal(20,2) NOT NULL DEFAULT '0.00',
			prod_weight decimal(20,2) NOT NULL DEFAULT '0.00',
			prod_desc text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
			prod_gst decimal(4,2) NOT NULL DEFAULT '1.00' COMMENT 'if checked the value will be 1.06',
			prod_gst_code varchar(10) DEFAULT '' COMMENT 'if empty the company will be no gst',
			prod_buy_price decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT 'product costs',
			prod_sell_price decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT 'product sell',
			prod_ship_fee decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT 'product shipping fees',
			prod_profit decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT 'product profit',
			prod_final_sell_price decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT 'product final sell price',
			prod_qty int(11) NOT NULL DEFAULT '-1' COMMENT '-1 = unlimited',
			prod_condition tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 = new, 1 = used, 2 = refurbished',
			prod_date_add datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
			prod_date_upd datetime NOT NULL DEFAULT '0000-00-00 00:00:00')",

		"CREATE TABLE tblimage (
			img_id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
			img_code varchar(18) NOT NULL DEFAULT '' COMMENT 'for order_num or prod_code or user_id',
			img_name varchar(50) NOT NULL DEFAULT '')",

		"CREATE TABLE tblcategory (
			cat_id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
			cat_name varchar(50) NOT NULL DEFAULT '',
			cat_subname varchar(50) NOT NULL DEFAULT '')",

		"CREATE TABLE tblcart (
			cart_id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
			prod_code varchar(18) NOT NULL,
			user_email varchar(50) NOT NULL COMMENT 'buyer email',
			cart_prod_qty int(11) NOT NULL DEFAULT '1',
			cart_prod_weight decimal(20,2) NOT NULL DEFAULT '0.00',
			cart_sub_total decimal(10,2) NOT NULL DEFAULT '0.00',
			cart_date_add datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
			cart_date_upd datetime NOT NULL DEFAULT '0000-00-00 00:00:00')",

		"CREATE TABLE tblorder (
			order_id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
			order_num varchar(18) NOT NULL DEFAULT '000000000000000000' COMMENT 'combined with year, month, day, hour, mins, sec and micro sec',
			prod_code varchar(18) NOT NULL,
			user_email varchar(50) NOT NULL COMMENT 'buyer email',
			order_prod_qty int(11) NOT NULL DEFAULT '1',
			order_prod_weight decimal(20,2) NOT NULL DEFAULT '0.00',
			order_sub_total decimal(10,2) NOT NULL DEFAULT '0.00',
			order_status tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=Payment Pending, 1=rejected, 2=process, 3=shipped',
			order_date_add datetime NOT NULL DEFAULT '0000-00-00 00:00:00')",

		"CREATE TABLE tblship (
			ship_id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
			order_num varchar(18) NOT NULL,
			ship_weight decimal(10,2) NOT NULL DEFAULT '0.00' COMMENT 'the total weight of product shipped',
			ship_name varchar(50) NOT NULL COMMENT 'recipient name',
			ship_email varchar(50) NOT NULL COMMENT 'recipient email',
			ship_mphone varchar(20) NOT NULL COMMENT 'recipient mobile phone',
			ship_address varchar(255) NOT NULL COMMENT 'recipient address',
			ship_city varchar(20) NOT NULL COMMENT 'recipient city',
			ship_postcode varchar(7) NOT NULL DEFAULT '00000' COMMENT 'recipient postcode',
			ship_state varchar(30) NOT NULL COMMENT 'recipient state')",

		"CREATE TABLE tblcomment (
			comment_id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
			prod_code varchar(18) NOT NULL,
			comment_name varchar(50) NOT NULL COMMENT 'reviewer name',
			comment_email varchar(50) NOT NULL COMMENT 'reviewer email',
			comment_desc text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
			comment_date_add datetime NOT NULL DEFAULT '0000-00-00 00:00:00')",

		"CREATE TABLE tblwish (
			wish_id int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT,
			prod_code varchar(18) NOT NULL,
			user_email varchar(50) NOT NULL COMMENT 'buyer email',
			wish_date_add datetime NOT NULL DEFAULT '0000-00-00 00:00:00')"
	);

	$dbName = "dboncart";
	
	function CreateTable($tableQueryList, $newLink) {
		foreach($tableQueryList as $tableQuery) {
			mysql_query($tableQuery, $newLink);
		}
	}

	//connect to mySQL sever
	$dbLink = mysql_connect("localhost", "root", "") or die(mysql_error());
	
	if($dbLink) {
		//choose database
		if(mysql_select_db($dbName)) { //check the databese exists or not
			CreateTable($tableList, $dbLink);
		}
		else {
			$sqlQurey = "CREATE DATABASE ".$dbName; //create database
			$dbResult = mysql_query($sqlQurey, $dbLink); //to check database create successful or not
			
			mysql_select_db($dbName);
			CreateTable($tableList, $dbLink);
			
			$sqluser = "INSERT INTO tbluser(user_fullname, user_email, user_pass, user_ic, user_level, user_status) VALUES('onCart', 'admin@email.com', md5('admin'), '123456121234', 4, 3)";
			$sqluserres = mysql_query($sqluser, $dbLink);

			$sqlcat = 
			"INSERT INTO tblcategory(cat_name, cat_subname) VALUES
				('Outdoor furniture',	'Outdoor textiles'),
				('Outdoor furniture',	'Barbeques'),
				('Outdoor furniture',	'Lounging & relaxing furniture'),
				('Outdoor furniture',	'Outdoor cushions'),
				('Outdoor furniture',	'Outdoor dining furniture'),
				('Outdoor furniture',	'Outdoor flooring'),
				('Outdoor furniture',	'Outdoor organising'),
				('Outdoor furniture',	'Parasols & gazebos'),
				('Beds & mattresses',	'Bed legs'),
				('Beds & mattresses',	'Beds with storage'),
				('Beds & mattresses',	'Double beds'),
				('Beds & mattresses',	'Foam & latex mattresses'),
				('Beds & mattresses',	'Guest beds & day beds'),
				('Beds & mattresses',	'Headboards'),
				('Beds & mattresses',	'Loft beds & bunk beds'),
				('Beds & mattresses',	'Mattress & pillow protectors'),
				('Beds & mattresses',	'Mattress pads'),
				('Beds & mattresses',	'Single beds'),
				('Beds & mattresses',	'Slatted bed bases'),
				('Beds & mattresses',	'Sofa-beds'),
				('Beds & mattresses',	'Spring mattresses'),
				('Chairs',	'Dining chairs'),
				('Chairs',	'Armchairs'),
				('Chairs',	'Bar chairs'),
				('Chairs',	'Highchairs'),
				('Chairs',	'Junior chairs'),
				('Chairs',	'Office chairs'),
				('Chairs',	'Step stools & step ladders'),
				('Chairs',	'Stools & benches'),
				('Tables',	'Dining tables'),
				('Tables',	'Bedside tables'),
				('Tables',	'Bar tables'),
				('Tables',	'Dressing tables'),
				('Cooking',	'Bakeware'),
				('Cooking',	'Cooking accessories'),
				('Cooking',	'Dish-washing accessories'),
				('Cooking',	'Food storage & organising'),
				('Cooking',	'Frying pans & woks'),
				('Cooking',	'Kitchen textiles'),
				('Cooking',	'Kitchen utensils'),
				('Cooking',	'Knives & chopping boards'),
				('Cooking',	'Mixing & measuring tools'),
				('Cooking',	'Ovenware'),
				('Cooking',	'Pots & sauce pans')";
			$sqlcatres = mysql_query($sqlcat, $dbLink);
			
			if($dbResult) {
				echo "<script>alert('Database created');</script>";
			}
		}
	}
?>