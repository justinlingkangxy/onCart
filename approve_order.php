<?php session_start(); ?>
<?php if(!empty($_SESSION['email']) && ($_SESSION['level'] == "admin" || $_SESSION['level'] == "employee")) { ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Approve Order :: onCart</title>
</head>

<?php
if(isset($_POST['btnsearch'])) {
	$attr = $_SERVER['QUERY_STRING'];
	if(!empty($_GET['pg'])) {
		$arr = explode('&pg=', $attr);
		$attr = $arr[0];
	}
	else if(!empty($_GET['search'])) {
		$arr = explode('&search=', $attr);
		$attr = $arr[0];
	}
	echo "<script>location='index.php?".$attr."&search=".$_POST['txtsearch']."';</script>";
}
?>

<div align="center">
<div class="products">
	<div class="container">
		<h1>Approve Order</h1>
	</div>
</div>
<form action="" method="post" accept-charset="utf-8">
	<div class="row">
		<div class="col-md-2"></div>
		<div class="col-md-8">
				<div class="pull-right"><input type="text" name="txtsearch" value="" placeholder="Order Number"> <input type="submit" name="btnsearch" value="Search" class="btn btn-info btn-sm"></div> <br/>
		</div>
		<div class="col-md-2"></div>
	</div>
	<br/>
	<table class="table">
		<thead>
			<tr>
				<th>No.</th>
				<th>Date Added</th>
				<th>Order Number</th>
				<th>Order By</th>
				<th>Status</th>
				<th>Action</th>
			</tr>
		</thead>
		<tbody id="table_content">
			<?php
				if($_GET['pg'] == "") {
					$page = 0;
				}
				else {
					$page = ($_GET['pg']*10)-10;
				}
				$res = "SELECT * FROM tblorder WHERE order_status < 1";
				if(!empty($_GET['search'])) {
					$res .= " AND order_num LIKE '%".$_GET['search']."%'";
				}
				$res .= " GROUP BY order_num ORDER BY order_date_add DESC";
				$checkres = mysql_query($res, $dbLink);
				$num = mysql_num_rows($checkres);
				$max_page = ceil($num/10);
				$res .= " LIMIT ".$page.",10";
				$checkres = mysql_query($res, $dbLink);
				if(mysql_num_rows($checkres) > 0) {
					for($i=0; $i<mysql_num_rows($checkres); $i++) {
						$reg = mysql_fetch_array($checkres);
						$date_time = explode(' ', $reg['order_date_add']);
						$date = $date_time[0];
						$time = $date_time[1];
						if($reg['order_status'] == 0) {
							$ostatus = "Payment Pending";
						}
						else if($reg['order_status'] == 1) {
							$ostatus = "Order Rejected";
						}
						else if($reg['order_status'] == 2) {
							$ostatus = "Process";
						}
						else if($reg['order_status'] == 3) {
							$ostatus = "Shipped";
						}
			?>
						<tr>
							<td><?php echo ($i+1)."."; ?></td>
							<td><span title="<?php echo $time; ?>"><?php echo $date; ?></span></td>
							<td><?php echo $reg['order_num']; ?></td>
							<?php
							$userres = "SELECT user_fullname FROM tbluser WHERE user_email = '".$reg['user_email']."'";
							$checkuserres = mysql_query($userres, $dbLink);
							$userreg = mysql_fetch_array($checkuserres);
							?>
							<td title="<?php echo $reg['user_email']; ?>"><?php echo $userreg['user_fullname']; ?></td>
							<td><?php echo $ostatus; ?></td>
							<td>
								<a class="btn btn-info btn-xs" href="index.php?id=admin_order_detail&oid=<?php echo $reg['order_num']; ?>" title="View Details"><i class="fa fa-list-alt"></i></a>
								<?php if($reg['order_status'] == 0) { ?>
									<button class="btn btn-danger btn-xs" title="Reject" name="btnreject" onclick="return confirm('Are you sure want to reject?')" value="<?php echo $reg['order_num']; ?>"><i class="fa fa-ban"></i></button>
									<button class="btn btn-success btn-xs" title="Approve" name="btnapprove" value="<?php echo $reg['order_num']; ?>"><i class="fa fa-check"></i></button>
								<?php } ?>
							</td>
						</tr>
			<?php 	}
				}
				else {
					echo "<tr><td></td><td></td><td></td><td>No Record(s) Found.</td><td></td><td></td></tr>";
				}
			?>
		</tbody>
	</table>
	</div> <div class='clearfix'> </div>
	<?php if($max_page > 1) {
		if(!empty($_GET['search'])) $search = "&search=".$_GET['search'];
	?>
	<div align="center">
		<ul class="pagination">
			<li><a href="index.php?id=apr_order&pg=1<?php echo $search ?>" <?php if($_GET['pg'] == 1 || empty($_GET['pg'])) echo "class='not-active'"; ?>><i class="fa fa-angle-double-left"></i></a></li>
			<li><a href="<?php if(empty($_GET['pg']) || $_GET['pg'] == 1) echo 'index.php?id=apr_order&pg=1'.$search; else echo 'index.php?id=apr_order&pg='.($_GET['pg']-1).$search; ?>" <?php if($_GET['pg'] == 1 || empty($_GET['pg'])) echo "class='not-active'"; ?>><i class="fa fa-angle-left"></i></a></li>
			<?php
				for($i=1; $i<=$max_page; $i++) {
					if(empty($_GET['pg']) && $i == 1) {
						echo "<li><a class='active' href='index.php?id=apr_order&pg=".$i.$search."'>".$i."</a></li>";
					}
					else if(!empty($_GET['pg']) && $_GET['pg'] == $i) {
						echo "<li><a class='active' href='index.php?id=apr_order&pg=".$i.$search."'>".$i."</a></li>";
					}
					else {
						echo "<li><a href='index.php?id=apr_order&pg=".$i.$search."'>".$i."</a></li>";
					}
				}
			?>
			<li><a href="<?php if(empty($_GET['pg']) || $_GET['pg'] == 1) echo 'index.php?id=apr_order&pg=2'.$search; else echo 'index.php?id=apr_order&pg='.($_GET['pg']+1).$search; ?>" <?php if($_GET['pg'] == $max_page) echo "class='not-active'"; ?>><i class="fa fa-angle-right"></i></a></li>
			<li><a href="index.php?id=apr_order&pg=<?php echo $max_page ?><?php echo $search ?>" <?php if($_GET['pg'] == $max_page) echo "class='not-active'"; ?>><i class="fa fa-angle-double-right"></i></a></li>
		</ul>
	</div>
	<?php } ?>
</form>
</div>

<?php
	if(isset($_POST['btnreject'])) {
		$updateOrder = "UPDATE tblorder SET order_status = '1' WHERE order_num = '".$_POST['btnreject']."'";
		$updateResult = mysql_query($updateOrder, $dbLink);
		if($updateResult) {
			$getprodqty = "SELECT prod_code, order_prod_qty FROM tblorder WHERE order_num = '".$_POST['btnreject']."'";
			$getprodqtyResult = mysql_query($getprodqty, $dbLink);
			for($i=0; $i<mysql_num_rows($getprodqtyResult); $i++) {
				$getprodqtyreg = mysql_fetch_array($getprodqtyResult);

				$getqty = "SELECT prod_qty FROM tblproduct WHERE prod_code = '".$getprodqtyreg['prod_code']."'";
				$getqtyres = mysql_query($getqty, $dbLink);
				$getqtyreg = mysql_fetch_array($getqtyres);
				if($getqtyreg['prod_qty'] != '-1') {
					$updateProd = "UPDATE tblproduct SET prod_qty = prod_qty + ".$getprodqtyreg['order_prod_qty']." WHERE prod_code = '".$getprodqtyreg['prod_code']."'";
					$updateProdResult = mysql_query($updateProd, $dbLink);
				}
			}
			echo "<script>alert('Reject Successfully!'); location='index.php?id=apr_order';</script>";
		}
		else
			echo "<script>alert('Reject Failed!');</script>";
	}
	if(isset($_POST['btnapprove'])) {
		$updateOrder = "UPDATE tblorder SET order_status = '2' WHERE order_num = '".$_POST['btnapprove']."'";
		$updateResult = mysql_query($updateOrder, $dbLink);
		if($updateResult) 
			echo "<script>alert('Approve Successfully!'); location='index.php?id=apr_order';</script>";
		else
			echo "<script>alert('Approve Failed!');</script>";
	}
?>
<?php
}
else {
	echo "<script>alert('Access Denied'); location='index.php';</script>";
}
?>