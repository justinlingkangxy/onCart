<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
	<link href="css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<script src="js/jquery.min.js"></script>
	<script src="js/totop.js"></script>
	<link href="css/style.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/totop.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/menu.css" rel="stylesheet" type="text/css" media="all" />
	<link href="css/comment.css" rel="stylesheet" type="text/css" media="all" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<link href="plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){window.scrollTo(0,1); } </script>
	<script src="js/responsiveslides.min.js"></script>
	<script>
		$(function () {
			$("#slider").responsiveSlides({
				auto: true,
				speed: 500,
				namespace: "callbacks",
				pager: true,
			});
		});
	</script>
</head>
<body>
<a href="#" class="crunchify-top" title="Go to Top"><i class="fa fa-arrow-up"></i></a>
<?php
	date_default_timezone_set("Asia/Kuala_Lumpur");
	include("database.php");
	include("constant.php");
	$comaddr = "SELECT user_address, user_city, user_postcode, user_state, user_country, user_mphone FROM tbluser WHERE user_email = 'admin@email.com'";
	$comaddrres = mysql_query($comaddr, $dbLink);
	$comaddrreg = mysql_fetch_array($comaddrres);
?>

<form action="" method="post" name="form_main" id="form_main">
<div class="header">
	<div class="header-top">
		<div class="container">
			<div class="col-sm-4 world"></div>

			<div class="col-sm-4 logo">
				<a href="index.php"><img style="max-width:250px;max-height:auto;" src="images/logo.png" alt=""></a>
			</div>
		
			<div class="col-sm-4">
				<?php 
				if($_GET['id'] == 'logout') {
					session_destroy();
					echo "<script>location='index.php?id=login';</script>";
				}
				else if(!empty($_SESSION['email'])) { ?>
					<nav>
						<ul class="nav">
							<li><a href="#"><?php echo $_SESSION['email'] ?></a>
								<ul>
									<li><a href="index.php?id=profile">Profile</a></li>
									<li><a href="index.php?id=acc">Account</a></li>
									<li><a href="index.php?id=list_order">My Order</a></li>
									<li><a href="index.php?id=favourite">My Favourite</a></li>
									<li><a href="index.php?id=logout">Log Out</a></li>
								</ul>
							</li>
						</ul>
					</nav>
				<?php } else { ?>
					<p class="log"><a href="index.php?id=login"> Login </a><span>or</span><a href="index.php?id=signup"> Signup </a></p>
				<?php } ?>
			</div>

			<div class="clearfix"></div>
		</div>
	</div>

	<div class="container">
		<div class="head-top">
			<div class="col-sm-2 number">
				<span><i class="glyphicon glyphicon-phone"></i><?php echo $comaddrreg['user_mphone']; ?></span>
			</div>
		 	<div class="col-sm-8">
			<nav>
				<ul class="nav">
				<li><a href="index.php">Home</a></li>
				<li><a href="#">All Products</a><ul>
				<?php
					$catres = "SELECT cat_name FROM tblcategory GROUP BY cat_name";
					$checkcatres = mysql_query($catres, $dbLink);
					while($row = mysql_fetch_array($checkcatres))
					{
						echo "<li><a href='#'>".$row['cat_name']."</a><ul>";
						$subcatres = "SELECT cat_id, cat_subname FROM tblcategory WHERE cat_name = '".$row['cat_name']."'";
						$checksubcatres = mysql_query($subcatres, $dbLink);
						while($subrow = mysql_fetch_array($checksubcatres))
						{
							echo "<li><a href='index.php?id=prod&cat=".$subrow['cat_id']."'>".$subrow['cat_subname']."</a></li>";
						}
						echo "</ul></li>";
					}
				?>
				</ul></li>
				<?php if(!empty($_SESSION['email']) && ($_SESSION['level'] == "seller" || $_SESSION['level'] == "admin")) { ?>
					<li><a href="#">Option</a>
						<ul>
							<li><a href="#">Product Management</a>
								<ul>
									<li><a href="index.php?id=reg_prod">Register Product</a></li>
									<li><a href="index.php?id=list_prod">List Product</a></li>
								</ul>
							</li>
							<li><a href="index.php?id=sales_report&y=<?php echo date('Y'); ?>">My Sales Report</a></li>
							<li><a href="index.php?id=topsales&y=<?php echo date('Y'); ?>">My Top Sales</a></li>
						</ul>	
					</li>
				<?php } ?>
				<?php if(!empty($_SESSION['email']) && ($_SESSION['level'] == "admin" || $_SESSION['level'] == "employee")) { ?>
					<li><a href="#"><?php if($_SESSION['level'] == "admin") echo "Admin "; else echo "Employee "; ?>Option</a>
						<ul>
							<?php if(!empty($_SESSION['email']) && $_SESSION['level'] == "admin") { ?>
							<li><a href="#">Employee Management</a>
								<ul>
									<li><a href="index.php?id=emp_profile&mode=add">Add Employee</a></li>
									<li><a href="index.php?id=list_emp">List Employee</a></li>
								</ul>
							</li>
							<li><a href="#">Category Management</a>
								<ul>
									<li><a href="index.php?id=customize_cat&mode=add">Add Category</a></li>
									<li><a href="index.php?id=customize_cat&mode=list">List Category</a></li>
								</ul>
							</li>
							<?php } ?>
							<li><a href="#">Approvement</a>
								<ul>
									<li><a href="index.php?id=apr_cmpy">Approve Company</a></li>
									<li><a href="index.php?id=apr_order">Approve Order</a></li>
								</ul>
							</li>
							<li><a href="#">Reports</a>
								<ul>
									<li><a href="index.php?id=user_summary&y=<?php echo date('Y'); ?>">User Summary Report</a></li>
									<li><a href="index.php?id=sales_report&mode=cust&y=<?php echo date('Y'); ?>">Customer Order</a></li>
									<li><a href="index.php?id=admin_sales_report&y=<?php echo date('Y'); ?>">Seller's Sales Report</a></li>
									<li><a href="index.php?id=admin_topsales&y=<?php echo date('Y'); ?>">Seller's Top Sales</a></li>
									<li><a href="index.php?id=commission&y=<?php echo date('Y'); ?>">Commission Report</a></li>
								</ul>
							</li>
						</ul>
					</li>
				<?php } ?>
				</ul>
			</nav>
			</div>
			<div class="col-sm-2 header-left">
				<?php
					$countcart = "SELECT count(cart_id) AS count_cart FROM tblcart WHERE user_email='".$_SESSION['email']."'";
					$checkcountcart = mysql_query($countcart, $dbLink);
					$count_cart = mysql_fetch_array($checkcountcart);
					if(isset($_POST['btnempty'])) {
						$delcart = "DELETE FROM tblcart WHERE user_email='".$_SESSION['email']."'";
						$sqldelcart = mysql_query($delcart, $dbLink);
						echo "<script>location='index.php?".$_SERVER['QUERY_STRING']."';</script>";
					}
				?>
				<div class="cart box_1">
					<a href="index.php?id=checkout" style="text-decoration:none">
						<h3>
							Cart <i class="fa fa-shopping-cart"></i>
							<span class="badge"><?php echo $count_cart['count_cart']; ?></span>
						</h3>
					</a>
					<p><button name="btnempty">Empty Cart</button></p>
				</div>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</div>
</form>

<?php
if($_GET['id'] == "ship_detail") {
	include ('shipping_detail.php');
}
else if($_GET['id'] == "topsales") {
	include ('topsales.php');
}
else if($_GET['id'] == "admin_topsales") {
	include ('admin_topsales.php');
}
else if($_GET['id'] == "commission") {
	include ('commission.php');
}
else if($_GET['id'] == "customize_cat") {
	include ('customize_cat.php');
}
else if($_GET['id'] == "user_summary_details") {
	include ('user_summary_details.php');
}
else if($_GET['id'] == "user_summary") {
	include ('user_summary.php');
}
else if($_GET['id'] == "sales_report_details") {
	include ('sales_report_details.php');
}
else if($_GET['id'] == "sales_report") {
	include ('sales_report.php');
}
else if($_GET['id'] == "admin_sales_report") {
	include ('admin_sales_report.php');
}
else if($_GET['id'] == "apr_order") {
	include ('approve_order.php');
}
else if($_GET['id'] == "payment") {
	include ('payment.php');
}
else if($_GET['id'] == "invoice") {
	include ('invoice.php');
}
else if($_GET['id'] == "admin_order_detail") {
	include ('admin_order_detail.php');
}
else if($_GET['id'] == "order_detail") {
	include ('order_detail.php');
}
else if($_GET['id'] == "list_order") {
	include ('list_order.php');
}
else if($_GET['id'] == "edit_prod") {
	include ('register_product.php');
}
else if($_GET['id'] == "single") {
	include ('single.php');
}
else if($_GET['id'] == "reg_prod") {
	include ('register_product.php');
}
else if($_GET['id'] == "emp_profile") {
	include ('emp_profile.php');
}
else if($_GET['id'] == "list_emp") {
	include ('list_emp.php');
}
else if($_GET['id'] == "list_prod") {
	include ('list_product.php');
}
else if($_GET['id'] == "discount") {
	include ('discount.php');
}
else if($_GET['id'] == "apr_cmpy") {
	include ('approve_company.php');
}
else if($_GET['id'] == "prod") {
	include ('products.php');
}
else if($_GET['id'] == "login") {
	include ('login.php');
}
else if($_GET['id'] == "signup") {
	include ('register.php');
}
else if($_GET['id'] == "favourite") {
	include ('favourite.php');
}
else if($_GET['id'] == "checkout") {
	include ('checkout.php');
}
else if($_GET['id'] == "acc") {
	include ('account.php');
}
else if($_GET['id'] == "cust_profile") {
	include ('cust_profile.php');
}
else if($_GET['id'] == "comprofile") {
	include ('company_profile.php');
}
else if($_GET['id'] == "profile") {
	include ('profile.php');
}
else if($_GET['id'] == "terms_conditions") {
	include ('terms_conditions.php');
}
else {
	include('main.php');
}
?>

<div class="footer">
	<div class="footer-bottom">
		<div class="container">
			<div class="col-sm-3 footer-bottom-cate">
				<h6>Guide</h6>
				<ul>
					<li><a href="index.php?id=terms_conditions&c=1">Making A Purchase</a></li>
					<li><a href="index.php?id=terms_conditions&c=2">Privacy Policy</a></li>
					<li><a href="index.php?id=terms_conditions&c=3">Start Your Business</a></li>
					<li><a href="index.php?id=terms_conditions&c=4">Delivery Policy</a></li>
				</ul>
			</div>
			<div class="col-sm-3 footer-bottom-cate"> </div>
			<div class="col-sm-3 footer-bottom-cate"> </div>
			<div class="col-sm-3 footer-bottom-cate cate-bottom">
				<h6>Our Address</h6>
				<ul>
					<li><?php echo $comaddrreg['user_address']; ?></li>
					<li><?php echo $comaddrreg['user_postcode']." ".$comaddrreg['user_city']; ?></li>
					<li><?php echo $comaddrreg['user_state']; ?></li>
					<li><?php echo $comaddrreg['user_country']; ?></li>
					<li class="phone"><i class="fa fa-mobile"></i> : <?php echo $comaddrreg['user_mphone']; ?></li>
				</ul>
			</div>
			<div class="clearfix"></div>
		</div>
	</div>
</div>

</body>
</html>