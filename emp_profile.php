<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Employee Profile :: onCart</title>
</head>

<?php
	$res = "SELECT * FROM tbluser WHERE user_id = '".$_GET['uid']."'";
	$checkres = mysql_query($res, $dbLink);
	if ($checkres) {
		$reg = mysql_fetch_array($checkres);
	}
?>

<?php
	if($_POST['gender'] == "Male") {
		$gender = 1;
	}
	else {
		$gender = 2;
	}
	
	if($_POST['btnadd']) {
		$res = "INSERT INTO tbluser(user_fullname, user_ic, user_email, user_mphone, user_pass, user_address, user_city, user_postcode, user_state, user_gender, user_birthdate, user_level, user_status, user_register_date) VALUES('".ucwords(trim($_POST['txtname']))."', '".trim($_POST['txtic'])."', '".trim($_POST['txtemail'])."', '".trim($_POST['txtmobile'])."', '".md5(trim($_POST['txtic']))."', '".trim($_POST['txtaddr'])."', '".trim($_POST['txtcity'])."', '".trim($_POST['txtpostcode'])."', '".trim($_POST['sel_state'])."', '".$gender."', '".$_POST['txtdate']."', '3', '3', '".date("Y-m-d H:i:s")."')";
		$checkres = mysql_query($res, $dbLink);
		if($checkres) {
			echo "<script>alert('Add successfully.'); location = 'index.php?".$_SERVER['QUERY_STRING']."';</script>";
		}
	}
	
	if($_POST['btnupdate']) {
		$res = "UPDATE tbluser SET user_fullname = '".ucwords(trim($_POST['txtname']))."', user_ic = '".trim($_POST['txtic'])."', user_mphone = '".trim($_POST['txtmobile'])."', user_address = '".trim($_POST['txtaddr'])."', user_city = '".trim($_POST['txtcity'])."', user_postcode = '".trim($_POST['txtpostcode'])."', user_state = '".trim($_POST['sel_state'])."', user_gender = '".$gender."', user_birthdate = '".$_POST['txtdate']."', user_status = '".trim($_POST['sel_status'])."' WHERE user_email = '".$_POST['txtemail']."'";
		$checkres = mysql_query($res, $dbLink);
		if($checkres) {
			echo "<script>alert('Update successfully.'); location = 'index.php?".$_SERVER['QUERY_STRING']."';</script>";
		}
	}
?>

<div class="container">
	<div class="register">
		<h1>Employee Profile</h1>
		<?php if($_GET['mode'] == 'edit') { ?>
		<form id="form_emp_img" name="form_emp_img" method="post" action="" enctype="multipart/form-data">
			<div align="center">
				<?php
					$getimg = "SELECT img_name FROM tblimage WHERE img_code = '".$_GET['uid']."'";
					$getimgResult = mysql_query($getimg, $dbLink);
					if(mysql_num_rows($getimgResult) > 0) {
						$img_name = mysql_fetch_array($getimgResult);
						echo "<div align='center'><img src='profile_images/".$img_name['img_name']."' width='auto' height='200px' alt=''></div>";
					}
					else {
						echo "<div align='center'><img src='images/no_image.jpg' width='auto' height='200px' alt=''></div>";
					}
				?>
				<div class="mation">
					<input type="file" name="img" value="" placeholder="">
					<div class="register-but">
						<input type="submit" name="btnupload" value="Upload">
					</div>
				</div>
				<div class="clearfix"> </div>
				<?php
				if(isset($_POST['btnupload'])) {
					echo $file_name = $_FILES['img']['name'];

					if($file_name != "") {
						$file_type = $_FILES['img']['type'];
						$allow_ext = array("jpg", "jpeg", "png", "gif");
						$ext = end(explode(".", $file_name));

						if(in_array(strtolower($ext), $allow_ext)) { //check file is invalid type*/
							$file_size = $_FILES['img']['size'];

							if($file_size < 10000000) { //check file is less than 10MB
								$file_new_name = $_GET['uid'].".".$ext; //rename file
								$file_tmp_name = $_FILES['img']['tmp_name'];
								$path = "profile_images/".$file_new_name;

								if(move_uploaded_file($file_tmp_name, $path)) {
									$upimg = "INSERT INTO tblimage(img_name, img_code) VALUES('".$file_new_name."', '".$_GET['uid']."')";
									$upimgResult = mysql_query($upimg, $dbLink);
									echo "<script>location='index.php?".$_SERVER['QUERY_STRING']."';</script>";
								}
								else {
									echo "<script>alert('Image upload failed!');</script>";
								}
							}
							else {
								echo "<script>alert('Image should be less than 10MB!');</script>";
							}
						}
						else {
							echo "<script>alert('Invalid image type!');</script>";
						}
					}
					else {
						echo "<script>alert('Please select an image!');</script>";
					}
				}
				?>
			</div>
			<div class="clearfix"> </div>
		</form>
		<?php } ?>
		<form id="form_profile" name="form_profile" method="post" action="">
			<div class="col-md-6 register-top-grid">
				<div class="mation">
					<span id="text_get">Employee's Name</span>
					<input type="text" name="txtname" required="required" maxlength="50" value="<?php echo $reg['user_fullname']; ?>" />
				 
					<span>NRIC</span>
					<input type="text" maxlength="16" placeholder="NNNNNN-NN-NNNN" name="txtic" value="<?php echo $reg['user_ic']; ?>" />
				 
					<span>Email Address</span>
					<input type="email" name="txtemail" required="required" maxlength="50" <?php if($_GET['mode'] == 'edit') echo "readonly='readonly'"; ?> value="<?php echo $reg['user_email']; ?>" />

					<span>Gender</span>
					<div class="col-md-6">
						<input type="radio" name="gender" id="rd1" class="acc_type" value="Male" <?php if($reg['user_gender'] == "1") echo "checked='checked'"; ?> /><label for="rd1" class="radio">Male</label>
					</div>
					<div class="col-md-6">
						<input type="radio" name="gender" id="rd2" class="acc_type" value="Female" <?php if($reg['user_gender'] == "2") echo "checked='checked'"; ?> /><label for="rd2" class="radio">Female</label>
					</div>

					<?php
						$birthdate = explode("-", $reg['user_birthdate']);
						$day = (int)$birthdate[2];
						$month = (int)$birthdate[1];
						$year = (int)$birthdate[0];
					?>

					<span>Date of Birth</span>
					<input type="date" name="txtdate" required="required" value="<?php echo $reg['user_birthdate']; ?>" />

				</div>
				<div class="clearfix"> </div>
			</div>
			<div class=" col-md-6 register-bottom-grid">
				<div class="mation">
					<span>Mobile Phone Number</span>
					<input type="text" name="txtmobile" required="required" maxlength="20" placeholder="0123456789" value="<?php echo $reg['user_mphone']; ?>" />

					<span>Address</span>
					<input type="text" name="txtaddr" required="required" maxlength="255" value="<?php echo $reg['user_address']; ?>" />

					<div class="col-md-6">
						<span>City</span>
						<input type="text" name="txtcity" required="required" maxlength="20" value="<?php echo $reg['user_city']; ?>" />
					</div>

					<div class="col-md-6">
						<span>Postcode</span>
						<input type="text" name="txtpostcode" required="required" maxlength="5" value="<?php echo $reg['user_postcode']; ?>" />
					</div>

					<?php if($_GET['mode'] == 'edit') echo "<div class='col-md-6'>"; ?>
					<span>State</span>
					<select class="form-group-lg form-control" style="margin-top: 5px;" name="sel_state">
						<option value="Unknown State">Please Select...</option>
						<?php
						for($i=0; $i<count($states); $i++) {
							if($reg['user_state'] == $states[$i]) {
								echo "<option selected='selected' value='".$states[$i]."'>".$states[$i]."</option>";
							}
							else {
								echo "<option value='".$states[$i]."'>".$states[$i]."</option>";
							}
						}
						?>
					</select>
					<?php if($_GET['mode'] == 'edit') echo "</div>"; ?>
					<?php if($_GET['mode'] == 'edit') { ?>
						<div class="col-md-6">
							<span>Status</span>
							<select class="form-group-lg form-control" style="margin-top: 5px;" name="sel_status">
								<option value="3" <?php if($reg['user_status'] == "3") echo "selected='selected'"; ?> >Active</option>
								<option value="-1" <?php if($reg['user_status'] == "-1") echo "selected='selected'"; ?> >Inactive</option>
							</select>
						</div>
					<?php } ?>
				</div>
			</div>
			<div class="clearfix"> </div>
				
			<div class="register-but">
				<input type="submit" <?php if($_GET['mode'] == 'edit') {echo "value='Update' name='btnupdate'";} else {echo "value='Add' name='btnadd'";} ?> >
				<div class="clearfix"> </div>
			</div>
		</form>
	</div>
</div>