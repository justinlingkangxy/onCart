<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>My Profile :: onCart</title>
</head>

<?php
	$res = "SELECT * FROM tbluser WHERE user_email = '".$_SESSION['email']."'";
	$checkres = mysql_query($res, $dbLink);
	if ($checkres) {
		$reg = mysql_fetch_array($checkres);
	}
?>

<?php
	if($_POST['gender'] == "Male" || $_POST['gender'] == "") {
		$gender = 1;
	}
	else {
		$gender = 2;
	}
	
	if($_POST['btnupdate']) {
		$res = "UPDATE tbluser SET user_fullname = '".ucwords(trim($_POST['txtname']))."', user_ic = '".trim($_POST['txtic'])."', user_mphone = '".trim($_POST['txtmobile'])."', user_address = '".trim($_POST['txtaddr'])."', user_city = '".trim($_POST['txtcity'])."', user_postcode = '".trim($_POST['txtpostcode'])."', user_state = '".trim($_POST['sel_state'])."', user_gender = '".$gender."', user_birthdate = '".$_POST['txtdate']."', user_gst_code = '".trim($_POST['txtgst'])."', user_contact_person = '".ucwords(trim($_POST['txtcp']))."', user_contact_person_phone = '".trim($_POST['txtcpnum'])."' WHERE user_email = '".$_SESSION['email']."'";
		$checkres = mysql_query($res, $dbLink);
		if($checkres) {
			echo "<script>alert('Update successfully.'); location = 'index.php';</script>";
		}
	}
?>

<div class="container">
	<div class="register">
		<h1>Profile</h1>
		<form id="form_profile_img" name="form_profile_img" method="post" action="" enctype="multipart/form-data">
			<div align="center">
				<?php
					$getimg = "SELECT img_name FROM tblimage WHERE img_code = '".$reg['user_id']."'";
					$getimgResult = mysql_query($getimg, $dbLink);
					if(mysql_num_rows($getimgResult) > 0) {
						$img_name = mysql_fetch_array($getimgResult);
						echo "<div align='center'><img src='profile_images/".$img_name['img_name']."' width='auto' height='200px' alt=''></div>";
					}
					else {
						echo "<div align='center'><img src='images/no_image.jpg' width='auto' height='200px' alt=''></div>";
					}
				?>
				<div class="mation">
					<input type="file" name="img" value="" placeholder="">
					<div class="register-but">
						<input type="submit" name="btnupload" value="Upload">
					</div>
				</div>
				<div class="clearfix"> </div>
				<?php
				if(isset($_POST['btnupload'])) {
					$file_name = $_FILES['img']['name'];

					if($file_name != "") {
						$file_type = $_FILES['img']['type'];
						$allow_ext = array("jpg", "jpeg", "png", "gif");
						$ext = end(explode(".", $file_name));

						if(in_array(strtolower($ext), $allow_ext)) { //check file is invalid type*/
							$file_size = $_FILES['img']['size'];

							if($file_size < 10000000) { //check file is less than 10MB
								$file_new_name = $reg['user_id'].".".$ext; //rename file
								echo $file_tmp_name = $_FILES['img']['tmp_name'];
								$path = "profile_images/".$file_new_name;

								if(move_uploaded_file($file_tmp_name, $path)) {
									$upimg = "INSERT INTO tblimage(img_name, img_code) VALUES('".$file_new_name."', '".$reg['user_id']."')";
									$upimgResult = mysql_query($upimg, $dbLink);
									echo "<script>location='index.php?".$_SERVER['QUERY_STRING']."';</script>";
								}
								else {
									echo "<script>alert('Image upload failed!');</script>";
								}
							}
							else {
								echo "<script>alert('Image should be less than 10MB!');</script>";
							}
						}
						else {
							echo "<script>alert('Invalid image type!');</script>";
						}
					}
					else {
						echo "<script>alert('Please select an image!');</script>";
					}
				}
				?>
			</div>
			<div class="clearfix"> </div>
		</form>
		<form id="form_profile" name="form_profile" method="post" action="">
			<div class="col-md-6 register-top-grid">
				<div class="mation">
					<span id="text_get"><?php if($reg['user_level'] == 2) echo "Company or Seller Name"; else if($reg['user_level'] == 3) echo "Employee Name"; else if($reg['user_level'] == 4) echo "Company Name"; else echo "Full Name"; ?></span>
					<input type="text" name="txtname" required="required" maxlength="50" value="<?php echo $reg['user_fullname']; ?>" />
				 
					<span><?php if($reg['user_level'] == 2 || $reg['user_level'] == 4) echo "Business License"; else echo "NRIC"; ?></span>
					<input type="text" name="txtic" maxlength="16" <?php if($reg['user_level'] == 0 || $reg['user_level'] == 1) echo "placeholder='NNNNNN-NN-NNNN'"; ?> value="<?php echo $reg['user_ic']; ?>" />
				 
					<span>Email Address</span>
					<input type="email" name="txtemail" readonly="readonly" maxlength="255" value="<?php echo $reg['user_email']; ?>" />

					<?php if($reg['user_level'] == 2 || $reg['user_level'] == 4) { ?>
						<span>Contact Person</span>
						<input type="text" name="txtcp" maxlength="50" value="<?php echo $reg['user_contact_person']; ?>" />

						<span>Contact Person Number</span>
						<input type="text" name="txtcpnum" maxlength="20" placeholder="0123456789" value="<?php echo $reg['user_contact_person_phone']; ?>" />
					<?php } else { ?>
						<span>Gender</span>
						<div class="col-md-6">
							<input type="radio" name="gender" id="rd1" class="acc_type" value="Male" <?php if($reg['user_gender'] == "1") echo "checked='checked'"; ?> /><label for="rd1" class="radio">Male</label>
						</div>
						<div class="col-md-6">
							<input type="radio" name="gender" id="rd2" class="acc_type" value="Female" <?php if($reg['user_gender'] == "2") echo "checked='checked'"; ?> /><label for="rd2" class="radio">Female</label>
						</div>

					<?php
						$birthdate = explode("-", $reg['user_birthdate']);
						$day = (int)$birthdate[2];
						$month = (int)$birthdate[1];
						$year = (int)$birthdate[0];
					?>

						<span>Date of Birth</span>
						<input type="date" name="txtdate" value="<?php echo $reg['user_birthdate']; ?>" />
					<?php } ?>

				</div>
				<div class="clearfix"> </div>
			</div>
			<div class=" col-md-6 register-bottom-grid">
				<div class="mation">
					<span><?php if($reg['user_level'] == 2 || $reg['user_level'] == 4) echo "Contact Number"; else echo "Mobile Phone Number"; ?></span>
					<input type="text" name="txtmobile" maxlength="20" value="<?php echo $reg['user_mphone']; ?>" />

					<?php if($reg['user_level'] == 2 || $reg['user_level'] == 4) { ?>
						<span>GST Registration Number</span>
						<input type="text" name="txtgst" maxlength="10" value="<?php echo $reg['user_gst_code']; ?>" />
					<?php } ?>

					<span>Address</span>
					<input type="text" name="txtaddr" required="required" maxlength="255" value="<?php echo $reg['user_address']; ?>" />

					<div class="col-md-6">
						<span>City</span>
						<input type="text" name="txtcity" required="required" maxlength="20" value="<?php echo $reg['user_city']; ?>" />
					</div>

					<div class="col-md-6">
						<span>Postcode</span>
						<input type="text" name="txtpostcode" required="required" maxlength="5" value="<?php echo $reg['user_postcode']; ?>" />
					</div>

					<span>State</span>
					<select class="form-group-lg form-control" style="margin-top: 5px;" name="sel_state">
						<option value="Unknown State">Please Select...</option>
						<?php
						for($i=0; $i<count($states); $i++) {
							if($reg['user_state'] == $states[$i]) {
						    	echo "<option selected='selected' value='".$states[$i]."'>".$states[$i]."</option>";
						    }
							else {
								echo "<option value='".$states[$i]."'>".$states[$i]."</option>";
							}
						}
						?>
   					</select>
				</div>
			</div>
			<div class="clearfix"> </div>
				
			<div class="register-but">
				<input type="submit" value="Update" name="btnupdate">
				<div class="clearfix"> </div>
			</div>
		</form>
	</div>
</div>