<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Customer's Profile :: onCart</title>
</head>

<?php
	$res = "SELECT * FROM tbluser WHERE user_id = '".$_GET['uid']."'";
	$checkres = mysql_query($res, $dbLink);
	if ($checkres) {
		$reg = mysql_fetch_array($checkres);
	}
?>

<div class="container">
	<div class="register">
		<h1><?php echo $reg['user_fullname'] ?> Profile</h1>
		<form id="form_profile" name="form_profile" method="post" action="">
			<div class="col-md-6 register-top-grid">
				<div class="mation">
					<span id="text_get"><?php if($reg['user_level'] == 2) echo "Company or Seller Name"; else if($reg['user_level'] == 3) echo "Employee Name"; else echo "Full Name"; ?></span>
					<input type="text" name="txtname" readonly="readonly" value="<?php echo $reg['user_fullname']; ?>" />
				 
					<span><?php if($reg['user_level'] == 2) echo "Business License"; else echo "NRIC"; ?></span>
					<input type="text" name="txtic" readonly="readonly" value="<?php echo $reg['user_ic']; ?>" />
				 
					<span>Email Address</span>
					<input type="email" name="txtemail" readonly="readonly" value="<?php echo $reg['user_email']; ?>" />

					<?php if($reg['user_level'] == 2) { ?>
						<span>Contact Person</span>
						<input type="text" name="txtcp" readonly="readonly" value="<?php echo $reg['user_contact_person']; ?>" />

						<span>Contact Person Number</span>
						<input type="text" name="txtcpnum" readonly="readonly" value="<?php echo $reg['user_contact_person_phone']; ?>" />
					<?php } else { ?>
						<span>Gender</span>
						<input type="text" name="txtgender" readonly="readonly" value="<?php if($reg['user_gender'] == "1") echo "Male"; else if($reg['user_gender'] == "2") echo "Female"; ?>" />
					<?php } ?>

				</div>
				<div class="clearfix"> </div>
			</div>
			<div class=" col-md-6 register-bottom-grid">
				<div class="mation">
					<span><?php if($reg['user_level'] == 2) echo "Contact Number"; else echo "Mobile Phone Number"; ?></span>
					<input type="text" name="txtmobile" readonly="readonly" value="<?php echo $reg['user_mphone']; ?>" />

					<?php if($reg['user_level'] == 2) { ?>
						<span>GST Registration Number</span>
						<input type="text" name="txtgst" readonly="readonly" value="<?php echo $reg['user_gst_code']; ?>" />
					<?php } ?>

					<span>Address</span>
					<input type="text" name="txtaddr" readonly="readonly" value="<?php echo $reg['user_address']; ?>" />

					<div class="col-md-6">
						<span>City</span>
						<input type="text" name="txtcity" readonly="readonly" value="<?php echo $reg['user_city']; ?>" />
					</div>

					<div class="col-md-6">
						<span>Postcode</span>
						<input type="text" name="txtpostcode" readonly="readonly" value="<?php echo $reg['user_postcode']; ?>" />
					</div>

					<span>State</span>
					<input type="text" name="txtstate" readonly="readonly" value="<?php echo $reg['user_state']; ?>" />
				</div>
			</div>
			<div class="clearfix"> </div>
		</form>
	</div>
</div>